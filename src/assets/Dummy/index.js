import Logo from './logo.png';
import Congrats from './congrats.png';
import Loading from './loading.png';
import Keys from './keys.png';
import Spiral from './spiral.png';
import User from './user.png';
import Circles from './circles.png';
import Discount from './discount.png';
import Card1 from './card1.png';
import Card2 from './card2.png';
import Card3 from './card3.png';
import Play from './play.png';
import Banner from './banner.png';
import BannerDetail from './bannerDetail.png';
import Peta from './peta.png';
import Barcode from './barcode.png';
import Alfamart from './alfamart.png';
import Indomart from './indomart.png';
import BRI from './bri.png';
import BCA from './bca.png';
import Mandiri from './mandiri.png';
import Dana from './dana.png';
import Ovo from './ovo.png';
import Gopay from './gopay.png';
import Check from './check.png';
import Trash from './trash.png';
import Ticket from './ticket.png';
import Logo2 from './logo2.png';
import Logout from './logout.png';
import umum1 from './umumone.png';
import umum2 from './umumtwo.png';
import umum3 from './umumthree.png';
import umum4 from './umumfour.png';
import callone from './callone.png';
import calltwo from './calltwo.png';
import callthree from './callthree.png';
import callfour from './callfour.png';
import faqone from './faqone.png';
import faqtwo from './faqtwo.png';
import faqthree from './faqthree.png';
import faqfour from './faqfour.png';

export {
    Logout,
    umum1,
    umum2,
    umum3,
    umum4,
    faqone,
    faqtwo,
    faqthree,
    faqfour,
    callone,
    calltwo,
    callthree,
    callfour,
    Logo2,
    Trash,
    Ticket,
    Check,
    Alfamart,
    Indomart,
    BRI,
    BCA,
    Mandiri,
    Dana,
    Ovo,
    Gopay,
    Peta,
    Barcode,
    BannerDetail,
    Banner,
    Play,
    Card1,
    Card2, 
    Card3,
    Discount,
    Circles,
    Logo,
    Congrats,
    Loading,
    Keys,
    Spiral,
    User
}