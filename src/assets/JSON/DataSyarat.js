export default [
    {description: 'Jika kamu melakukan pembelian e-tiket acara ini, maka dianggap sudah membaca, memahami, dan menyetujui seluruh sayarat & ketentuan dari acara.'},
    {description: 'Harga sudah termasuk pajak.'},
    {description: 'Tiket yang sudah dibeli tidak dapat dikembalikan dan non-refundable.'},
    {description: 'Tiket yang sudah dibeli tidak dapat diganti jadwalnya.'},
    {description: 'Pembeli wajib mengisi data diri saat memesan.'},
    {description: 'Pejualan tiket sewaktu-waktu dapat dihentikan atau dimulai oleh e-tiket sesuai kebijakan promotor.'},
    {description: 'Pengunjung wajib mematuhi protokol kesehatan dan keamanan yang berlaku.'},
    {description: 'Disarankan tidak membawa anak dibawah umur 12 tahun.'},
    {description: 'Jam buka dan jam mulai kegiatan dapat berubah sewaktu-waktu tanpa adanya pemberitahuan sebelumnya.'},
];









