export default [
    {
        title: 'Virtual Account',
        data: [
            {
                image: require('../../assets/Dummy/bca.png'),
                text: 'BCA Virtual Account',
                k: "bca"
            },
            {
                image: require('../../assets/Dummy/bri.png'),
                text: 'BRI Virtual Account',
                k: "bri"
            },
            {
                image: require('../../assets/Dummy/mandiri.png'),
                text: 'Mandiri Virtual Account',
                k: "mandiri"
            },
        ]
    },
    {
        title: 'Transfer Bank',
        data: [
            {
                image: require('../../assets/Dummy/bca.png'),
                text: 'BCA Virtual Account',
                k: "bcaTF"
            },
            {
                image: require('../../assets/Dummy/bri.png'),
                text: 'BRI Virtual Account',
                k: "briTF"
            },
            {
                image: require('../../assets/Dummy/mandiri.png'),
                text: 'Mandiri Virtual Account',
                k: "mandiriTF"
            },
        ]
    },
    {
        title: 'E-Wallet',
        data: [
            {
                image: require('../../assets/Dummy/dana.png'),
                text: 'BCA Virtual Account',
                k: "dana"
            },
            {
                image: require('../../assets/Dummy/gopay.png'),
                text: 'BRI Virtual Account',
                k: "gopay"
            },
            {
                image: require('../../assets/Dummy/ovo.png'),
                text: 'Mandiri Virtual Account',
                k: "ovo"
            },
        ]
    },
    {
        title: 'Gerai Agen E-Tiket',
        data: [
            {
                image: require('../../assets/Dummy/indomart.png'),
                text: 'Indomart',
                k: "indomart"
            },
            {
                image: require('../../assets/Dummy/alfamart.png'),
                text: 'Alfamart',
                k: "alfamart"
            },
        ]
    }
];