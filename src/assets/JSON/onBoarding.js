export default [
    {
      id: '1',
      title: 'Be productive while working from home',
      description:
        'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.',
      image: require('../Dummy/OnBoarding1.png'),
    },
    {
      id: '2',
      title: 'Be productive while working from home',
      description:
        'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.',
      image: require('../Dummy/OnBoarding2.png'),
    },
    {
      id: '3',
      title: 'Be productive while working from home',
      description:
        'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.',
      image: require('../Dummy/OnBoarding3.png'),
    },
  ];
  