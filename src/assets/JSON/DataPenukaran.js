export default [
    {description: 'Seat Number untuk kategori VVIP, Diamond dan Platinum akan diberikan pada saat penukaran tiket.'},
    {description: 'Tunjukkan e-tiket yang telah diterima melaui email atau di aplikasi e-tiket kepada petugas lapangan untuk scan QR Code.'},
    {description: 'Wajib menunjukkan kartu identitas yang berlaku.'},
    {description: 'Jika penukaran tiket diwakilkan, maka wajib membawa surat kuasa atas nama pembeli dan kartu identitas pemberi dan penerima kuasa.'},
    {description: 'Setelah scan QR Code sukses terverifikasi, pelanggan akan mendapatkan wristband yang dapat digunakan untuk memasuki venue.'},
    {description: 'Pelanggan wajib mematuhi protokol kesahatan dan keamanan saat event sedang berlangsung.'},
    {description: 'Penukaran voucher pada tanggal 12 Desember 2023, pukul 13.00 WIB di area yang disediakan.'},
]