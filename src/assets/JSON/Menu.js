import React from 'react';
import {
  IcCovid,
  IcEvents,
  IcHiburan,
  IcOther,
  IcPermainan,
  IcTour,
} from '../Icons';

export default [
  {
    title: 'Permainan',
    image: require('../Icons/IcPermainan.png'),
    action: 'Permainan',
  },
  {
    title: 'Hiburan',
    image: require('../Icons/IcHiburan.png'),
    action: 'Hiburan',
  },
  {
    title: 'Tur',
    image: require('../Icons/IcTour.png'),
    action: 'Tur',
},
{
    title: 'Event',
    image: require('../Icons/IcEvents.png'),
    action: 'Event',
},
{
    title: 'Tes Covid-19',
    image: require('../Icons/IcCovid.png'),
    action: 'Tes',
},
{
    title: 'Perawatan',
    image: require('../Icons/IcOther.png'),
    action: 'Perawatan',
},
{
    title: 'Perawatan',
    image: require('../Icons/IcOther.png'),
    action: 'Perawatan',
},
{
    title: 'Perawatan',
    image: require('../Icons/IcOther.png'),
    action: 'Perawatan',
},
{
    title: 'Perawatan',
    image: require('../Icons/IcOther.png'),
    action: 'Perawatan',
},
{
    title: 'Perawatan',
    image: require('../Icons/IcOther.png'),
    action: 'Perawatan',
  },
];

