import {NativeModules, Platform} from 'react-native';

const {SplashScreen} = NativeModules;

function show() {
  if (Platform.OS === 'android') {
    SplashScreen.show();
  } else {
    SplashScreen.show();
  }
}

function hide() {
  if (Platform.OS === 'android') {
    SplashScreen.hide();
  } else {
    SplashScreen.hide();
  }
}

export default {
  show,
  hide,
};
