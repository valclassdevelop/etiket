import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors, scaleSize} from '../utils';
import {IcArrowLeft} from '../assets';
import {useNavigation} from '@react-navigation/native';
import Gap from './Gap';
import {TextBold, TextMedium} from './Text';

const Header = ({
  text,
  textColor = colors.Black,
  iconColor = "#115888",
  type,
  title = "Event",
  backgroundColor = 'transparent',
  onPressRight,
}) => {
  const navigation = useNavigation();

  const onBack = () => {
    navigation.goBack();
  };

  return type === 'main' ? (
    <View style={styles.headerMain}>
     <Text style={styles.title}>{text}</Text>
    </View>
  ) : (
    <View style={[styles.header, {backgroundColor}]}>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={onBack}
        style={styles.backButton}>
        <IcArrowLeft stroke={iconColor} />
      </TouchableOpacity>
      <TextBold style={{marginLeft: 'auto', marginRight: 'auto', fontWeight: 'bold'}} type="Text Medium 18" text={text} color={textColor} />
    </View>
  );
};

export default React.memo(Header);

const styles = StyleSheet.create({  
  header: {
    padding: scaleSize(16),
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 40,
    zIndex: 1,
  },
  headerMain: {
    flexDirection: 'row',
    height: 'auto',
    padding: scaleSize(20),
    paddingTop: scaleSize(30)
  },
  backButton: {
    width: scaleSize(24),
    height: scaleSize(24),
  },
  title: {
    fontSize: scaleSize(22),
    fontWeight: 'bold',
    color: colors.Black
  },
});
