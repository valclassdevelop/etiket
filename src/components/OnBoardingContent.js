import React from 'react';
import {
  StyleSheet,
  View,
  Animated,
  useWindowDimensions,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image,
} from 'react-native';
import  IcChevronRight from '../assets/Icons/IcChevronRight.svg';
import {colors, scaleSize} from '../utils';
import Gap from './Gap';
import {TextBold, TextRegular} from './Text';

const OnBoardingContent = ({dots, data, scrollX, scrollTo, status}) => {
  const {width} = useWindowDimensions();
  return (
    <>
      <View style={[styles.container, {width}]}>
        <Image
          source={data?.image}
          style={[styles.image, {width}]}
          resizeMode="contain"
        />
        <TextBold
          type="Title Bold 32"
          text={data?.title}
          color={colors.White}
        style={styles.titleOnBoarding}
        />
        <Gap height={10} />
        <TextRegular
          type="Text Regular 16"
          text={data?.description}
          style={styles.paragpraphOnBoarding}
        />
        <Gap height={28} />
        <View style={styles.row}>
          {
            status ? (
              <View style={styles.pagination}>
                <View style={styles.wrapperDotsss}>
                  <View style={styles.dotsss}></View>
                  <View style={styles.dotsss}></View>
                  <View style={styles.dotsss}></View>
                </View>
              </View>
            ):
            <View style={styles.pagination}>
              {dots.map((_, i) => {
                const inputRange = [(i - 1) * width, i * width, i * width];
                const dotWidth = scrollX.interpolate({
                  inputRange,
                  outputRange: [10, 20, 10],
                  extrapolate: 'clamp',
                });
                const opacity = scrollX.interpolate({
                  inputRange,
                  outputRange: [0.5, 1, 0.5],
                  extrapolate: 'clamp',
                });
                return (
                  <Animated.View
                    key={i.toString()}
                    style={[styles.dot, {width: dotWidth, opacity}]}
                  />
                );
              })}
            </View>
          }
          {
            status ? (
              <TouchableOpacity activeOpacity={0.8} style={styles.btnEnd} onPress={scrollTo}>
                <TextBold text="Get Started" type="Text Bold 14" color={colors.White} />  
              </TouchableOpacity>
            ):
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.button}
              onPress={scrollTo}>
              <IcChevronRight stroke={colors.White} />
            </TouchableOpacity>
          }
        </View>
      </View>
    </>
  );
};

export default React.memo(OnBoardingContent);

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: colors.White,
    paddingTop: scaleSize(80),
  },
  titleOnBoarding: {
    marginLeft: 24,
    marginTop: 34,
    color: '#115888', 
    fontFamily: "Inter-Bold",
    fontWeight: 'bold'
  },
  paragpraphOnBoarding: {
    color: '#434F65',
    marginLeft: 24,
    fontFamily: "Inter-Regular",
    width: '90%',
    fontSize: 16,
    marginTop: 20,
  },
  image: {
    height: scaleSize(260),
  },
  background: {
    width: Dimensions.get('window').width,
    height: scaleSize(400),
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: -52,
    paddingHorizontal: scaleSize(20),
    paddingVertical: scaleSize(138),
    backgroundColor: colors.White,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 20,
    marginTop: 40,
    marginRight: 20,
  },
  pagination: {
    flexDirection: 'row',
  },
  dot: {
    height: scaleSize(8),
    borderRadius: scaleSize(8),
    backgroundColor: '#115888',
    marginHorizontal: scaleSize(6),
  },
  button: {
    backgroundColor: '#115888',
    height: scaleSize(48),
    width: scaleSize(48),
    borderRadius: scaleSize(32),
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnEnd: {
    backgroundColor: 'rgba(52, 168, 83, 1)',
    borderRadius: scaleSize(999),
    paddingVertical: scaleSize(14),
    paddingHorizontal: scaleSize(26),
    alignItems: 'center',
    justifyContent: 'center'
  },
  wrapperDotsss: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dotsss: {
    borderRadius: scaleSize(20),
    backgroundColor: 'rgba(52, 168, 83, 1)',
    width: scaleSize(8),
    marginRight: scaleSize(8),
    height: scaleSize(8)
  }
});
