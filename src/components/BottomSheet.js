import React from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import {colors, scaleSize} from '../utils';
import Modal from 'react-native-modal';

export const BottomSheet = ({children, isVisible, onClose, height = 'auto'}) => {
  return (
    <Modal
      useNativeDriver
      isVisible={isVisible}
      style={styles.container}
      animationIn="slideInUp"
      animationOut="slideOutDown"
      animationInTiming={800}
      animationOutTiming={400}
      backdropColor={colors.Black}
      backdropOpacity={0.6}
      onBackdropPress={() => {
        onClose();
      }}
      onBackButtonPress={() => {
        onClose();
      }}
      transparent={true}
      hideModalContentWhileAnimating={true}>
      <View style={[styles.wrapper, {height: height}]}>{children}</View>
    </Modal>
  );
};

export const BottomSheetCenter = ({children, isVisibleCenter, onClose, height = Dimensions.get('window').height * 0.7 }) => {
  return (
    <Modal
      useNativeDriver
      isVisible={isVisibleCenter}
      style={styles.container}
      animationIn="slideInUp"
      animationOut="slideOutDown"
      animationInTiming={800}
      animationOutTiming={400}
      backdropColor={colors.Black}
      backdropOpacity={0.6}
      onBackdropPress={() => {
        onClose();
      }}
      onBackButtonPress={() => {
        onClose();
      }}
      transparent={true}
      hideModalContentWhileAnimating={true}>
      <View style={[styles.wrapper2, {height: height}]}>{children}</View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 0,
    justifyContent: 'center',
  },
  wrapper: {
    top: 255,
    borderRadius: 20,
    backgroundColor: colors.White,
    borderTopLeftRadius: scaleSize(16),
    borderTopRightRadius: scaleSize(16),
  },
  wrapper2: {
    top: 22,
    width: '80%',
    overflow: 'hidden',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 20,
    backgroundColor: colors.White,
    borderTopLeftRadius: scaleSize(16),
    borderTopRightRadius: scaleSize(16),
  },
});