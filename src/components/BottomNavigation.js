import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {IcEtiket, IcEventtt, IcHistory, IcHome, IcHomepage, IcUser} from '../assets';
import {TextMedium, TextRegular} from './Text';
import Gap from './Gap';
import {colors, scaleSize} from '../utils';

const Icon = ({label, active}) => {
  switch (label) {
    case 'Homepage':
      return active ? (
        <IcHomepage width={19} fill={colors['DeepDenim-550']} />
      ) : (
        <IcHomepage width={19} fill={colors['RitualCyan-400']} />
      );
    case 'Event':
      return active ? (
          <IcEventtt fill={colors['DeepDenim-550']} />
        ) : (
          <IcEventtt fill={colors['RitualCyan-400']} />
        );
    case 'Etiket':
      return active ? (
        <IcEtiket fill={colors['DeepDenim-550']} />
      ) : (
        <IcEtiket fill={colors['RitualCyan-400']} />
      );
    case 'Transaksi':
      return active ? (
        <IcHistory fill={colors['DeepDenim-550']} />
      ) : (
        <IcHistory fill={colors['RitualCyan-400']} />
      );
    case 'Profile':
      return active ? (
        <IcUser fill={colors['DeepDenim-550']} />
      ) : (
        <IcUser fill={colors['RitualCyan-400']} />
      );

    default:
      return <IcHome fill={colors['DeepDenim-550']} />;
  }

};

const BottomNavigation = ({state, descriptors, navigation}) => {

  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View>
      <View style={styles.container}>
        {state.routes.map((route, index) => {
          const {options} = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          return (
            <TouchableOpacity
              key={index}
              activeOpacity={0.7}
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              style={styles.menu}
              onPress={onPress}
              onLongPress={onLongPress}>
              <Icon label={label} active={isFocused} />
              <Gap height={8} />
              {isFocused ? (
                <TextMedium
                type="Text Regular 10"
                text={label}
                    color="rgba(19, 129, 182, 1)"
                  />
                  ) : (
                    <TextRegular
                    type="Text Regular 10"
                    text={label}
                    color="rgba(182, 198, 227, 1)"
                />
              )}
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};

export default BottomNavigation;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    backgroundColor: colors.White,
    justifyContent: 'space-between',
    paddingHorizontal: scaleSize(30),
    elevation: 10,
  },
  menu: {
    height: scaleSize(48),
    marginVertical: scaleSize(16),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
