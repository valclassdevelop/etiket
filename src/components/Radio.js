import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {colors, scaleSize} from '../utils';

const Radio = ({isCheck, onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.checkBox}
      onPress={onPress}>
      {isCheck && <View style={styles.check} />}
    </TouchableOpacity>
  );
};

export default Radio;

const styles = StyleSheet.create({
  checkBox: {
    width: scaleSize(20),
    height: scaleSize(20),
    borderWidth: 1,
    borderColor: colors['DeepDenim-550'],
    borderRadius: scaleSize(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
  check: {
    backgroundColor: colors['DeepDenim-550'],
    width: scaleSize(12),
    height: scaleSize(12),
    borderRadius: scaleSize(12),
  },
});
