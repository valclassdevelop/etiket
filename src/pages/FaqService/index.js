import React, { useState } from 'react';
import { ScrollView, LayoutAnimation, FlatList, StatusBar, StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import { IcChevronDown, IcChevronUp } from '../../assets';
import DataCollapse2 from '../../assets/JSON/DataCollapse2';
import { Gap, Divider, Header, TextBold, TextRegular } from '../../components';
import { colors, scaleSize } from '../../utils';

const FaqService = ({navigation}) => {

    const [isShow, setIsShow] = useState(null);

    const handleAccordion = item => {
      if (item?.id === isShow) {
        LayoutAnimation.easeInEaseOut();
        setIsShow(null);
      } else {
        LayoutAnimation.easeInEaseOut();
        setIsShow(item?.id);
      }
    };

    const listMenu = [
        {
            text: 'Umum',
            img: require('../../assets/Dummy/faqone.png'),
        },
        {
            text: 'Akun',
            img: require('../../assets/Dummy/faqtwo.png'),
        },
        {
            text: 'Pemesanan',
            img: require('../../assets/Dummy/faqthree.png'),
        },
        {
            text: 'Pembayaran',
            img: require('../../assets/Dummy/faqfour.png'),
        },
        {
            text: 'Lainnya',
            img: require('../../assets/Dummy/faqone.png'),
        },
        {
            text: 'Lainnya',
            img: require('../../assets/Dummy/faqone.png'),
        },
    ]

    const renderItem = ({item}) => {
        return (
            <View style={styles.cardMenu}>
                <View style={styles.imgCard}>
                    <Image source={item?.img} alt="img-menu" style={{width: scaleSize(40), height: scaleSize(40)}} />
                </View>
                <Gap height={10} />
                <TextRegular text={item?.text} type="Text Regular 14" color="rgba(0, 0, 0, 1)" />
            </View>
        )
    }

    return (
        <ScrollView style={styles.container}>
            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <Header text="FAQ" onBack={() => navigation.goBack(-1)} />
            <Gap height={24} />
            <View style={styles.wrapperMenu}>
                <TextBold text="Pilih Topik Bantuan" type="Text Bold 16" color={colors.Black} style={{marginLeft: scaleSize(20)}} />
                <Gap height={20} />
                <FlatList 
                    data={listMenu}
                    renderItem={renderItem}
                    numColumns={3}
                    ItemSeparatorComponent={() => <View height={20} />}
                />
            </View>
            <Gap height={20} />
            <Divider height={1} />
            <Gap height={20} />
            {
                    DataCollapse2.map((item) => {
                        return (
                            <TouchableOpacity key={item?.id} activeOpacity={0.8} style={styles.cardDrop} onPress={() => handleAccordion(item)}>
                            <View style={styles.cardDropTop}>
                            <TextBold color={colors.Black} style={styles.titleDrop}  text={item?.title} />
                                {isShow === item?.id ? (
                                    <IcChevronUp fill="rgba(19, 129, 182, 1)" width={14} height={14} />

                                ) : (
                                    <IcChevronDown fill="rgba(19, 129, 182, 1)" width={14} height={14} />
                                )}
                            </View>
                            {isShow === item?.id && (
                                <>
                                    <Gap  height={20} />
                                    <TextRegular 
                                        text={item?.desc}
                                        type="Text Regular 12" />
                                </>
                            )}
                       </TouchableOpacity>
                        )
                    })
                }
            <Gap height={20} />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White
    },
    cardMenu: {
        width: '33%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgCard: {
        width: scaleSize(60),
        height: scaleSize(60),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.White,
        borderRadius: scaleSize(16),
        elevation: scaleSize(3)
    },
    cardDrop: {
        marginHorizontal: scaleSize(20),
        backgroundColor: colors.White,
        paddingVertical: scaleSize(20),
        elevation: 3,
        padding: scaleSize(14),
        marginBottom: scaleSize(20),
        borderRadius: scaleSize(8)
    },
    cardDropTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    titleDrop: {
        width: '80%'
    }
});

export default FaqService;