import React, {useState} from 'react';
import {
  StyleSheet,
  FlatList,
  ScrollView,
  TextInput,
  View,
  StatusBar,
  TouchableOpacity,
  Image,
} from 'react-native';
import {IcFilter2, IcSearch} from '../../assets';
import {Banner} from '../../assets/Dummy';
import {Divider, Gap, Header, TextBold, TextRegular} from '../../components';
import {colors, NumberFormatter, scaleSize} from '../../utils';

const Transaksi = ({navigation}) => {
  const [active, setActive] = useState('Semua');
  const dataMenu = ['Semua', 'Selesai', 'Dibatalkan'];

  return (
    <ScrollView>
      <StatusBar barStyle="dark-content" backgroundColor={colors.White} />
      <Header type="main" text="Transaksi" />
      <View style={styles.wrapperSearchEtiket}>
        <View style={styles.search}>
          <TextInput placeholder="Cari pesanan" style={{width: '80%'}} />
          <IcSearch width={18} height={18} stroke="rgba(182, 198, 227, 1)" />
        </View>
        <Gap width={10} />
        <TouchableOpacity activeOpacity={0.8} style={styles.filter}>
          <TextBold
            text="Filter"
            color="rgba(19, 129, 182, 1)"
            type="Text Bold 14"
          />
          <Gap width={4} />
          <IcFilter2 width={16} height={16} />
        </TouchableOpacity>
      </View>
      <Gap height={10} />
      <View style={styles.wrapperMenu}>
        <FlatList
          data={dataMenu}
          numColumns={3}
          columnWrapperStyle={{justifyContent: 'space-between'}}
          renderItem={({item}) => {
            return (
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => setActive(item)}
                style={styles.childMenu(active, item)}>
                <TextRegular
                  text={item}
                  type="Text Regular 14"
                  color={
                    active === item ? colors.White : 'rgba(67, 79, 101, 1)'
                  }
                />
              </TouchableOpacity>
            );
          }}
        />
      </View>
      <Gap height={20} />
      <View style={styles.cardEtiket}>
        <View style={styles.top}>
          <View style={styles.cardEtiketLeft}>
            <TextRegular
              text="ID:"
              color={colors.Black}
              type="Text Regular 14"
            />
            <TextRegular
              text="#0987654"
              color={colors.Black}
              type="Text Regular 14"
            />
          </View>
          <View style={styles.cardEtiketRight}>
            <TextRegular
              text="Selesai"
              color="rgba(52, 168, 83, 1)"
              type="Text Regular 12"
            />
          </View>
        </View>
        <Gap height={4} />
        <Divider height={1} />
        <Gap height={6} />
        <View style={styles.selectTiket}>
          <View style={styles.imgSelectTiket}>
            <Image
              source={Banner}
              alt="img"
              style={{width: '100%', height: '100%'}}
              resizeMode="cover"
            />
          </View>
          <Gap width={10} />
          <View style={styles.rightSelect}>
            <TextRegular
              text="Produk yang dipesan"
              color="rgba(67, 79, 101, 1)"
              type="Text Regular 12"
            />
            <Gap height={8} />
            <View style={styles.wrapperTitle}>
              <View style={styles.titleSelectTiket}>
                <TextBold
                  text="National Musif Festival"
                  color="#000"
                  type="Text Bold 14"
                />
              </View>
              <TextBold
                text={NumberFormatter(1500000, 'IDR ')}
                color="rgba(19, 129, 182, 1)"
                style={styles.prc}
                type="Text Bold 14"
              />
            </View>
          </View>
        </View>
        <Gap height={10} />
        <View style={styles.descSelect}>
          <TextRegular text="Tanggal & Waktu" type="Text Regular 12" />
          <TextBold color="#000" text="30 January 2023" type="Text Bold 12" />
        </View>
        <Gap height={14} />
        <View style={styles.descSelect}>
          <TextRegular text="Lokasi" type="Text Regular 12" />
          <TextBold
            color="#000"
            text="Jakarta, Indonesia"
            type="Text Bold 12"
          />
        </View>
        <Divider height={1} />
        <Gap height={14} />
        <View style={styles.footerDetailEvent}>
          <View style={styles.fdeLeft}>
            <TextRegular text="Total" style={styles.start} />
            <TextBold
              text={NumberFormatter(1500000, 'IDR ')}
              color="rgba(19, 129, 182, 1)"
              type="Text Bold 16"
            />
          </View>
          <TouchableOpacity activeOpacity={0.8} style={styles.btnfde}>
            <TextBold
              text="LIhat Detail"
              color={colors.White}
              type="Text Bold 12"
            />
          </TouchableOpacity>
        </View>
      </View>
      <Gap height={20} />
      <View style={styles.cardEtiket}>
        <View style={styles.top}>
          <View style={styles.cardEtiketLeft}>
            <TextRegular
              text="ID:"
              color={colors.Black}
              type="Text Regular 14"
            />
            <TextRegular
              text="#0987654"
              color={colors.Black}
              type="Text Regular 14"
            />
          </View>
          <View style={styles.cardEtiketRight}>
            <TextRegular
              text="Selesai"
              color="rgba(52, 168, 83, 1)"
              type="Text Regular 12"
            />
          </View>
        </View>
        <Gap height={4} />
        <Divider height={1} />
        <Gap height={6} />
        <View style={styles.selectTiket}>
          <View style={styles.imgSelectTiket}>
            <Image
              source={Banner}
              alt="img"
              style={{width: '100%', height: '100%'}}
              resizeMode="cover"
            />
          </View>
          <Gap width={10} />
          <View style={styles.rightSelect}>
            <TextRegular
              text="Produk yang dipesan"
              color="rgba(67, 79, 101, 1)"
              type="Text Regular 12"
            />
            <Gap height={8} />
            <View style={styles.wrapperTitle}>
              <View style={styles.titleSelectTiket}>
                <TextBold
                  text="National Musif Festival"
                  color="#000"
                  type="Text Bold 14"
                />
              </View>
              <TextBold
                text={NumberFormatter(1500000, 'IDR ')}
                color="rgba(19, 129, 182, 1)"
                style={styles.prc}
                type="Text Bold 14"
              />
            </View>
          </View>
        </View>
        <Gap height={10} />
        <View style={styles.descSelect}>
          <TextRegular text="Tanggal & Waktu" type="Text Regular 12" />
          <TextBold color="#000" text="30 January 2023" type="Text Bold 12" />
        </View>
        <Gap height={14} />
        <View style={styles.descSelect}>
          <TextRegular text="Lokasi" type="Text Regular 12" />
          <TextBold
            color="#000"
            text="Jakarta, Indonesia"
            type="Text Bold 12"
          />
        </View>
        <Divider height={1} />
        <Gap height={14} />
        <View style={styles.footerDetailEvent}>
          <View style={styles.fdeLeft}>
            <TextRegular text="Total" style={styles.start} />
            <TextBold
              text={NumberFormatter(1500000, 'IDR ')}
              color="rgba(19, 129, 182, 1)"
              type="Text Bold 16"
            />
          </View>
          <TouchableOpacity activeOpacity={0.8} style={styles.btnfde}>
            <TextBold
              text="LIhat Detail"
              color={colors.White}
              type="Text Bold 12"
            />
          </TouchableOpacity>
        </View>
      </View>
      <Gap height={110} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  wrapperSearchEtiket: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 'auto',
    paddingHorizontal: scaleSize(20),
    justifyContent: 'space-between',
    paddingVertical: scaleSize(10),
  },
  search: {
    width: '70%',
    flexDirection: 'row',
    borderRadius: scaleSize(8),
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: scaleSize(1),
    borderColor: 'rgba(182, 198, 227, 1)',
    paddingHorizontal: scaleSize(10),
  },
  filter: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'rgba(17, 88, 136, 0.1)',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: scaleSize(6),
    paddingVertical: scaleSize(16),
    borderRadius: scaleSize(8),
  },
  prc: {
    position: 'relative',
    marginTop: scaleSize(24),
  },
  childMenu: (active, item) => ({
    borderRadius: scaleSize(99),
    width: '31%',
    borderWidth: scaleSize(1),
    borderColor: 'rgba(235, 237, 241, 1)',
    paddingHorizontal: scaleSize(10),
    paddingVertical: scaleSize(6),
    alignItems: 'center',
    backgroundColor: active === item ? 'rgba(19, 129, 182, 1)' : colors.White,
  }),
  cardEtiket: {
    backgroundColor: colors.White,
    elevation: 3,
    marginHorizontal: scaleSize(20),
    borderRadius: scaleSize(8),
    height: 'auto',
    paddingBottom: scaleSize(10),
  },
  cardEtiketLeft: {
    width: 'auto',
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrapperMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 'auto',
    marginHorizontal: scaleSize(20),
  },
  cardEtiketRight: {
    borderRadius: scaleSize(88),
    backgroundColor: 'rgba(52, 168, 83, 0.1)',
    color: 'rgba(52, 168, 83, 1)',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: scaleSize(10),
    paddingVertical: scaleSize(4),
  },
  top: {
    flexDirection: 'row',
    padding: scaleSize(16),
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  selectTiket: {
    width: '90%',
    padding: scaleSize(10),
    backgroundColor: 'rgba(182, 198, 227, 0.15)',
    borderRadius: scaleSize(8),
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    overflow: 'hidden',
  },
  rightSelect: {
    width: '78%',
  },
  imgSelectTiket: {
    width: scaleSize(60),
    maxHeight: scaleSize(60),
    borderRadius: scaleSize(8),
    overflow: 'hidden',
  },
  wrapperTitle: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    paddingRight: scaleSize(8),
  },
  titleSelectTiket: {
    width: '50%',
    overflow: 'hidden',
  },
  descSelect: {
    justifyContent: 'space-between',
    marginHorizontal: scaleSize(20),
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerDetailEvent: {
    backgroundColor: colors.White,
    borderTopColor: '#EBEDF1',
    borderTopWidth: scaleSize(2),
    paddingHorizontal: scaleSize(20),
    paddingVertical: scaleSize(14),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  fdeLeft: {
    width: '60%',
  },
  btnfde: {
    width: scaleSize(110),
    paddingVertical: scaleSize(8),
    backgroundColor: '#1381B6',
    color: colors.White,
    borderRadius: scaleSize(8),
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3,
  },
  textBtnfde: {
    color: colors.White,
  },
  textDiscountPrice: {
    color: 'red',
    fontSize: scaleSize(14),
    marginBottom: scaleSize(8),
  },
  start: {
    color: 'rgba(67, 79, 101, 1)',
    fontSize: scaleSize(14),
    marginBottom: scaleSize(4),
  },
  wrapperInfo: {
    flexDirection: 'column',
  },
});

export default Transaksi;
