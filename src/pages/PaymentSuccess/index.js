import React, { useState } from 'react';
import { Animated, Dimensions, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { IcChevronDown, IcLocation, IcLocationBlack, IcMessageBlue } from '../../assets';
import { Banner, BCA, Check, Peta, Ticket, Trash } from '../../assets/Dummy';
import { Divider, FilledButton, Gap, Header, TextBold, TextInput, TextRegular } from '../../components';
import Radio from '../../components/Radio';
import { colors, NumberFormatter, scaleSize } from '../../utils';


const ContinuePayment = ({navigation}) => {
    const [activeStep1, setActiveStep1] = useState(false);
    const [activeStep2, setActiveStep2] = useState(false);
    const [activeStep3, setActiveStep3] = useState(false);

    return (
        <ScrollView style={styles.container}>
            <Header text="Pembayaran Berhasil" onBack={() => navigation.navigate('ContinuePayment2')} />
            <Gap height={20} />
           <TouchableOpacity style={styles.wrapperStep}>
                <TouchableOpacity activeOpacity={0.8} style={styles.step}>
                    <Image source={Check} alt="success" style={{width: scaleSize(20), height: scaleSize(20), marginRight: scaleSize(4)}} />
                    <TextRegular text="Pilih metode" type="Text Regular 12" />
                    <View style={{width: 20, height: scaleSize(1), backgroundColor: 'rgba(182, 198, 227, 1)', top: scaleSize(2), left: scaleSize(10)}} />
                </TouchableOpacity>
                <View>
                    <Divider height={2} />
                </View>
                <View style={[styles.step, {left: scaleSize(10)}]}>
                    <Image source={Check} alt="success" style={{width: scaleSize(20), height: scaleSize(20), marginRight: scaleSize(4)}} />
                    <TextRegular text="Bayar" type="Text Regular 12" />
                    <View style={{width: 20, height: scaleSize(1), backgroundColor: 'rgba(182, 198, 227, 1)', top: scaleSize(2), left: scaleSize(10)}} />
                </View>
                <View>
                    <Divider height={2} />
                </View>
                <View style={styles.step}>
                    <Image source={Check} alt="success" style={{width: scaleSize(20), height: scaleSize(20), marginRight: scaleSize(4)}} />
                    <TextRegular text="Selesai" type="Text Regular 12" />
                </View>
           </TouchableOpacity>
           <Gap height={24} />
           <View style={styles.cardSuccess}>
            <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('DetailEtiket')} style={styles.childSuccess}>
                <Image source={Ticket} alt="img" style={{width: scaleSize(24), height: scaleSize(24)}} />
                <TextRegular text="E-tiket" type="Text Regular 12" color="rgba(0, 0, 0, 1)" />
            </TouchableOpacity>
            <View style={styles.childSuccess}>
                <Image source={Trash} alt="img" style={{width: scaleSize(24), height: scaleSize(24)}} />
                <TextRegular text="Hapus" type="Text Regular 12" color="rgba(0, 0, 0, 1)" />
            </View>
           </View>
           <Gap height={24} />
           <View style={styles.cardCheck}>
                <Gap height={16} />
                <View style={styles.cardCheckBottom}>
                    <View style={styles.cardTop2}>
                        <View style={styles.imgCheckout}>
                            <Image source={Banner} alt="img-product" style={{width: '100%', maxHeight: '100%'}} resizeMode="cover" />
                        </View>
                        <Gap width={10} />
                        <View style={styles.cardTop3}>
                            <TextBold color={colors.Black} text="National Music Festival" type="Title Bold 14" />
                            <Gap height={2} />
                            <TextRegular text="Mon, Dec 23 - 18.00 - 23.00 PM" color="#115888" type="Text Regular 10" />
                            <Gap height={8} />
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <TouchableOpacity activeOpacity={0.8} style={styles.detail}>
                                    <TextRegular text="Detail" type="Text Regular 10" color="rgba(19, 129, 182, 1)" />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <Gap height={16} />
                    <FilledButton text="Pesan Lagi" backgroundColor='rgba(19, 129, 182, 1)' textColor={colors.White} />
                </View>
           </View>
           <Gap height={16} />
           <View style={styles.cardNext}>
                <TextBold text="Detail Paket" color={colors.Black} type="Text Bold 16" />
                <Gap height={2} />
                <TextRegular text="Event Music Festival" color="rgba(67, 79, 101, 1)" type="Text Regular 14" />
                <Gap height={16} />
                <TextBold text="Tanggal" color={colors.Black} type="Text Bold 16" />
                <Gap height={2} />
                <TextRegular text="Berlaku pada 30 Januari 2023" color="rgba(67, 79, 101, 1)" type="Text Regular 14" />
                <Gap height={16} />
                <TextBold text="Venue" color={colors.Black} type="Text Bold 16" />
                <Gap height={4} />
                <Image source={Banner} alt="img-detail" style={{width: 50, height: scaleSize(50), borderRadius: scaleSize(4)}} />
                <Gap height={16} />
                <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
                <TextBold text="Info Pemesan" color={colors.Black} type="Text Bold 16" />
                <TextRegular text="Detail" color="rgba(19, 129, 182, 1)" type="Text Regular 12" />
                </View>
                <Gap height={2} />
                <TextRegular text="Mr. Shohei" color="rgba(67, 79, 101, 1)" type="Text Regular 14" />
            </View>
            <Gap height={20} />
            <View style={styles.cardMap}>
                <Gap height={10} />
                <Text style={styles.titleDetailMap}>Lokasi</Text>
                <Gap height={14} />
                <View style={styles.peta}>
                    <Image source={Peta} alt="peta" style={{width: '100%', maxHeight: '100%'}} resizeMode="cover" />
                </View>
                <Gap height={14} />
                <View style={styles.wrapperLocation}>
                    <IcLocationBlack width={12} height={12} style={{marginRight: scaleSize(10)}} />
                    <Text style={styles.textLoc}>Jakarta, Indonesia</Text>
                </View>
                <Gap height={20} />
                <View style={styles.wrapperPetunjuk}>
                    <View style={styles.childPetunjuk}>
                        <View style={styles.bullet}>
                            <IcMessageBlue width={24} height={24} />
                        </View>
                        <Gap height={6} />
                        <Text style={{fontWeight: 500, color: '#434F65'}}>Petunjuk Arah</Text>
                    </View>
                    <View style={styles.childPetunjuk}>
                        <View style={styles.bullet}>
                            <IcLocation width={24} height={24} />
                        </View>
                        <Gap height={6} />
                        <Text style={{fontWeight: 500, color: '#434F65'}}>Panduan ke Lokasi</Text>
                    </View>
                </View>
            </View>
            <Divider height={4} />
            <View style={styles.footerDetailEvent}>
                <View style={styles.fdeLeft}>
                    <View style={styles.wrapperTopFooter}>
                        <TextRegular type="Text Regular 12" color="#434F65" text="Total" />
                        <TextRegular type="Text Regular 14" color="rgba(19, 129, 182, 1)" text="Bagikan bukti bayar" />
                    </View>
                    <Gap height={8} />
                    <TextBold text={NumberFormatter(1500000, 'IDR ')} type="Text Bold 18" color={colors.Black} />
                </View>
                <Gap height={16} />
            </View>
            <Divider height={4} />
            <View style={styles.help}>
                <TextRegular type="Text Regular 14" text="Butuh bantuan?" />
                <Gap width={6} />
                <TextBold type="Text Bold 14" color="rgba(19, 129, 182, 1)" text="Hubungi kami" />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: Dimensions.get('window').height,
        backgroundColor: colors.White
    },
    detail: {
        borderRadius: scaleSize(4),
        borderWidth: scaleSize(1),
        borderColor: 'rgba(19, 129, 182, 1)',
        alignItems: 'center',
        paddingVertical: scaleSize(4),
        paddingHorizontal: scaleSize(12),
        justifyContent: 'center'
    },
    cardTop2: {
        flexDirection: 'row',
        alignItems: 'center',
        height: scaleSize(80),
    },
    wrapperStep: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: scaleSize(20)   
    },
    step: {
        width: '33%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    numberStep: () => ({
        borderWidth: scaleSize(1),
        borderColor: '#434F65',
        alignItems: 'center',
        justifyContent: 'center',
        width: scaleSize(24),
        height: scaleSize(24),
        marginRight: scaleSize(4),
        borderRadius: scaleSize(90)
    }),
    titleDetailMap: {
        fontSize: scaleSize(20),
        fontWeight: 'bold',
        marginTop: scaleSize(4),
        color: colors.Black,
        marginLeft: scaleSize(20)
    },
    titleDetail2: {
        fontSize: scaleSize(20),
        fontWeight: 'bold',
        marginTop: scaleSize(20),
        color: colors.Black,
        marginLeft: scaleSize(20)
    },
    cardSuccess: {
        borderRadius: scaleSize(10),
        paddingHorizontal: scaleSize(14),
        paddingVertical: scaleSize(20),
        marginHorizontal: scaleSize(20),
        backgroundColor: colors.White,
        elevation: 3,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 'auto',
    },
    childSuccess: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%',
    },
    cardNext: {
        borderRadius: scaleSize(10),
        padding: scaleSize(14),
        marginHorizontal: scaleSize(20),
        backgroundColor: colors.White,
        elevation: 3,
        height: 'auto',
    },
    cardMap: {
        borderRadius: scaleSize(10),
        marginHorizontal: scaleSize(20),
        backgroundColor: colors.White,
        elevation: 3,
        height: 'auto',
        marginBottom: scaleSize(40)
    },
    cardCheck: {
        borderRadius: scaleSize(10),
        padding: scaleSize(14),
        marginHorizontal: scaleSize(20),
        backgroundColor: colors.White,
        elevation: 3,
        height: 'auto',
    },
    cardCheckTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: scaleSize(6)
    },
    wrapperTimes: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    childTimes: {
        width: scaleSize(22),
        height: scaleSize(22),
        borderRadius: scaleSize(4),
        backgroundColor: '#EB4335',
        alignItems: 'center',
        justifyContent: 'center',
    },
    imgCheckout: {
        width: scaleSize(80),
        height: '100%',
        borderRadius: scaleSize(10),
        overflow: 'hidden'
    },
    selectBank: {
        flexDirection: 'row',
        alignItems: 'center',
        height: scaleSize(50),
    },
    imgBank: {
        width: scaleSize(40),
        height: scaleSize(40),
        borderRadius: scaleSize(6),
        overflow: 'hidden',
    },
    total: {
        borderRadius: scaleSize(8),
        paddingVertical: scaleSize(12),
        backgroundColor: 'rgba(19, 129, 182, 0.1)',
        padding: scaleSize(20)
    },
    wrapperAccordion: {
        marginHorizontal: scaleSize(20),
        height: 'auto'
    },
    childAccordion: {
        elevation: 3,
        borderRadius: scaleSize(6),
        backgroundColor: colors.White,
        paddingVertical: scaleSize(20),
        paddingHorizontal: scaleSize(14),
        height: 'auto',
        marginBottom: scaleSize(16),
    },
    childAccordionTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    footerDetailEvent: {
        backgroundColor: colors.White,
        borderTopColor: '#EBEDF1',
        width: Dimensions.get('window').width,
        borderTopWidth: scaleSize(2),
        paddingHorizontal: scaleSize(20),
        paddingVertical: scaleSize(14),
        flexDirection: 'column',
    },
    fdeLeft: {
        width: '60%'
    },
    btnfde: {
        width: scaleSize(110),
        paddingVertical: scaleSize(8),
        backgroundColor: '#1381B6',
        color: colors.White,
        borderRadius: scaleSize(4),
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3
    },
    peta: {
        marginHorizontal: scaleSize(20),
        overflow: 'hidden',
        height: scaleSize(160),
        borderRadius: scaleSize(10)
    },
    wrapperLocation: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: scaleSize(20)
    },
    wrapperPetunjuk: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: scaleSize(120)
    },
    childPetunjuk: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bullet: {
        borderRadius: scaleSize(999),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: scaleSize(1),
        borderColor: '#1381B6',
        width: scaleSize(45),
        height: scaleSize(45),
    },
    wrapperTopFooter: {
        width: Dimensions.get('window').width * 0.9,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    help: {
        height: scaleSize(100),
        backgroundColor: colors.White,
        textAlign: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: scaleSize(20),
    }
});

export default ContinuePayment;