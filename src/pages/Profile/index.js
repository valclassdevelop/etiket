import React, { useState } from 'react';
import { Image, FlatList, ScrollView, StatusBar, StyleSheet, TouchableOpacity, View } from 'react-native';
import { IcChevronRight } from '../../assets';
import { Circles, Logo2, Spiral, User } from '../../assets/Dummy';
import { Gap, TextBold, TextRegular, Divider, BottomSheet, FilledButton } from '../../components';
import { colors, scaleSize } from '../../utils';

const Profile = ({navigation}) => {

    const [active, setActive] = useState(false);

    const listUmum = [
        {
            title: 'Update Password',
            img: require('../../assets/Dummy/umumone.png'),
            link: 'UpdatePassword',
        },
        {
            title: 'Hubungi Kami',
            img: require('../../assets/Dummy/umumtwo.png'),
            link: 'CallService',
        },
        {
            title: 'FAQ',
            img: require('../../assets/Dummy/umumthree.png'),
            link: 'FaqService',
        },
    ]
    
    const listAccount = [
        {
            title: 'Pesanan Saya',
            img: require('../../assets/Dummy/umumfour.png'),
            onPress: () => {
                navigation.navigate('MyOrder')
            },
        },
        {
            title: 'Keluar',
            img: require('../../assets/Dummy/logout.png'),
            onPress: () => {
                setActive(true)
            },
        },
    ]

    const renderItem = ({item}) => {
        return (
            <>
                <TouchableOpacity activeOpacity={0.8} style={styles.childUmum} onPress={() => navigation.navigate(item?.link)}>
                    <View style={styles.umumLeft}>
                        <View style={styles.iconUmum}>
                            <Image source={item?.img} alt="icon" style={{width: scaleSize(20), height: scaleSize(20)}} resizeMode="contain" />
                        </View>
                        <Gap width={10} />
                        <TextRegular text={item?.title} type="Text Regular 14" color="rgba(0, 0, 0, 1)" />
                    </View>
                    <IcChevronRight width={10} height={10} stroke="rgba(67, 79, 101, 1)" />
                </TouchableOpacity>
                <Gap height={20} />
                <Divider height={2} backgroundColor="rgba(243, 251, 255, 1)" />
                <Gap height={20} />
            </>
        )
    }
    const renderItem2 = ({item}) => {
        return (
            <>
                <TouchableOpacity activeOpacity={0.8} style={styles.childUmum} onPress={item?.onPress}>
                    <View style={styles.umumLeft}>
                        <View style={styles.iconUmum}>
                            <Image source={item?.img} alt="icon" style={{width: scaleSize(20), height: scaleSize(20)}} resizeMode="contain" />
                        </View>
                        <Gap width={10} />
                        <TextRegular text={item?.title} type="Text Regular 14" color="rgba(0, 0, 0, 1)" />
                    </View>
                    <IcChevronRight width={10} height={10} stroke="rgba(67, 79, 101, 1)" />
                </TouchableOpacity>
                <Gap height={20} />
                <Divider height={2} backgroundColor="rgba(243, 251, 255, 1)" />
                <Gap height={20} />
            </>
        )
    }

    return (
        <ScrollView>
              <StatusBar
                barStyle="light-content"
                backgroundColor="rgba(19, 129, 182, 1)"
            />
            <View style={styles.wrapperBanner}>
                <Image source={Spiral} alt="particel" style={styles.spiral1} />
                <Image source={Spiral} alt="particel" style={styles.spiral2} />
                <Image source={Circles} alt="particel" style={styles.circles1} />
                <View style={styles.titleBanner}>
                    <Image source={Logo2} alt="Logo" style={{width: scaleSize(32), height: scaleSize(32), borderRadius: scaleSize(6)}} resizeMode="cover" />
                    <Gap width={10} />
                    <TextBold text="Profile" color={colors.White} type="Title Bold 20" />
                </View>
                <View style={styles.wrapperName}>
                    <View style={styles.nameLeft}>
                        <View style={styles.imgUser}>
                            <Image source={User} resizeMode="cover" alt="user-photo" style={styles.fotoUser} />
                        </View>
                        <Gap width={10} />
                        <View>
                            <TextBold text="Shohei Ohtani" type="Text Bold 18" color={colors.White} />
                            <Gap height={4} />
                            <TextRegular text="shohei@address.com" color={colors.White} type="Text Regular 14" />
                        </View>
                    </View>
                    <TouchableOpacity activeOpacity={0.8} style={styles.edit} onPress={() => navigation.navigate('EditProfile')}>
                        <TextBold text="Edit" color={colors.White} type="Text Bold 12" />
                    </TouchableOpacity>
                </View>
                <Gap height={6} />
            </View>
            <View style={styles.listUmum}>
                <Gap height={20} />
                <TextBold text="Umum" type="Text Bold 16" color={colors.Black} />
                <Gap height={30} />
                <FlatList 
                    data={listUmum}
                    renderItem={renderItem}
                />
                <Gap height={20} />
                <TextBold text="Account" type="Text Bold 16" color={colors.Black} />
                <Gap height={30} />
                <FlatList 
                    data={listAccount}
                    renderItem={renderItem2}
                />
            </View>

            {/* Modal Logout */}
            <BottomSheet isVisible={active} onClose={() => setActive(false)}>
                <View style={styles.wrapperModal}>
                    <Gap height={12} />
                    <TextBold color="red" text="Keluar" type="Title Bold 24" />
                    <Gap height={12} />
                    <Divider height={2} />
                    <Gap height={12} />
                    <TextBold text="Kamu yakin ingin keluar akun?" type="Text Bold 14" />
                    <Gap height={30} />
                    <View style={styles.wrapperBtnModal}>
                        <View style={styles.wrapperBtn}>
                            <FilledButton text="Batal" onPress={() =>  setActive(false)} backgroundColor="rgba(19, 129, 182, 0.1)" textColor="rgba(19, 129, 182, 1)" />
                        </View>
                        <Gap width={12} />
                        <View style={styles.wrapperBtn}>
                            <FilledButton text="Ya, Logout" backgroundColor="rgba(19, 129, 182, 1)" textColor={colors.White} />
                        </View>
                    </View>
                </View>
                <Gap height={12} />
            </BottomSheet>
            <Gap height={60} />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderRightColor: colors.White
    },
    wrapperBanner: {
        height: 'auto',
        padding: scaleSize(24),
        backgroundColor: 'rgba(19, 129, 182, 1)',
        borderBottomLeftRadius: scaleSize(10),
        borderBottomRightRadius: scaleSize(10),
    },
    titleBanner: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: scaleSize(20),
    },
    spiral1: {
        position: 'absolute',
        top: scaleSize(-100),
        right: scaleSize(80),
        width: scaleSize(160),
        maxHeight: scaleSize(160)
    },
    spiral2: {
        position: 'absolute',
        width: scaleSize(240),
        bottom: scaleSize(-90),
        left: scaleSize(-90),
        maxHeight: scaleSize(240)
    },
    circles1: {
        position: 'absolute',
        width: scaleSize(120),
        right: scaleSize(-10),
        bottom: scaleSize(-10),
        maxHeight: scaleSize(120)
    },
    wrapperName: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: scaleSize(60),
    },
    nameLeft: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgUser: {
        width: scaleSize(60),
        height: scaleSize(60),
        borderRadius: scaleSize(988),
        overflow: 'hidden',
    },
    fotoUser: {
        width: '100%',
        maxHeight: '100%'
    },
    edit: {
        borderWidth: scaleSize(1),
        borderColor: colors.White,
        alignItems: 'center',
        borderRadius: scaleSize(99),
        justifyContent: 'center',
        paddingHorizontal: scaleSize(26),
        paddingVertical: scaleSize(10)
    },
    listUmum: {
        marginHorizontal: scaleSize(20),
        height: 'auto',
        marginTop: scaleSize(20)
    },
    childUmum: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    umumLeft: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconUmum: {
        backgroundColor: 'rgba(19, 129, 182, 0.1)',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: scaleSize(10),
        width: scaleSize(50),
        height: scaleSize(50),
    },
    wrapperBtnModal: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    wrapperBtn: {
        width: '45%'
    },
    wrapperModal: {
        padding: scaleSize(16),
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default Profile;