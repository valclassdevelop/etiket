import React, { useState } from 'react';
import { Image, ScrollView, SectionList, StyleSheet, TouchableOpacity, View } from 'react-native';
import DataVA from '../../assets/JSON/DataVA';
import { Gap, Header, TextBold } from '../../components';
import Radio from '../../components/Radio';
import { colors, scaleSize } from '../../utils';

const OtherPamtment = ({navigation}) => {
    const [active, setActive] = useState("");
    
    const renderItemVA = ({item}) => {
        return (
            <TouchableOpacity activeOpacity={0.86} style={styles.card} onPress={() => setActive(item?.k)}>
                <View style={styles.cardLeft}>
                    <View style={styles.cardLeftImg}>
                        <Image source={item?.image} alt="img" style={{width: '100%', height: '100%'}} resizeMode="contain" />
                    </View>
                    <Gap width={14} />
                    <TextBold text={item?.text} type="Title Bold 14" />
                </View>
                <Radio isCheck={active == item?.k ? true : false} />
            </TouchableOpacity>
        )
    }

    return (
        <ScrollView style={styles.container}>
            <Header text="Metode Pembayaran Lainnya" />
            <Gap height={20} />
            <SectionList
                sections={DataVA}
                renderSectionHeader={({section: {title}}) => (
                    <TextBold text={title} type="Text Bold 14" style={{marginLeft: scaleSize(20), marginVertical: scaleSize(16)}} color={colors.Black} />
                )}
                renderItem={renderItemVA}
            />
            <Gap height={20} />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    card: {
        flexDirection: 'row',
        marginBottom: scaleSize(12),
        alignItems: 'center',
        justifyContent: 'space-between',
        elevation: 1,
        borderRadius: scaleSize(10),
        marginHorizontal: scaleSize(20),
        paddingVertical: scaleSize(10),
        backgroundColor: colors.White,
        padding: scaleSize(14),
    },
    cardLeftImg: {
        width: scaleSize(50),
        height: scaleSize(50),
        borderRadius: scaleSize(6),
        overflow: 'hidden'
    },
    cardLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '80%',
    }
});

export default OtherPamtment;