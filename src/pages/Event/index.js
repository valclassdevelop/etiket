import React, { useState } from 'react';
import { FlatList, Image, ScrollView, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { IcCalendar, IcFilter, IcLocation, IcSearch, IcWallet } from '../../assets';
import { Banner } from '../../assets/Dummy';
import { FilledButton, Gap, Header, TextBold, TextRegular } from '../../components';
import { colors, NumberFormatter, scaleSize } from '../../utils';
import EventData from "../../assets/JSON/Promo";
import {BottomSheet} from '../../components/BottomSheet';
import Radio from '../../components/Radio';
import { Calendar } from 'react-native-calendars';

const Event = ({navigation}) => {

    const [activeStatus, setActiveStatus] = useState(false);
    const [activeHarga, setActiveHarga] = useState(false);
    const [activeRadio1, setActiveRadio1] = useState(false);
    const [activeRadio2, setActiveRadio2] = useState(false);
    const [activeRadioHarga1, setActiveRadioHarga1] = useState(false);
    const [activeRadioHarga2, setActiveRadioHarga2] = useState(false);
    const [activeModalFilter, setActiveModalFilter] = useState(false);

    const renderItem = ({item}) => {
        return (
            <TouchableOpacity activeOpacity={0.92} style={styles.cardMain} onPress={() => navigation.navigate('DetailEvent')}>
            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <View style={styles.cardMainTop}>
                <Image source={Banner} style={{width: '100%', maxHeight: '100%'}} resizeMode="contain" />
            </View>
            <View style={styles.cardMainBottom}>
                <TextBold text={item?.title} style={styles.titleCard} />
                <TextRegular text={item?.time} color="rgba(17, 88, 136, 1)" style={styles.textDate} />
            </View>
            <View style={styles.footerCardMain}>
                <View style={styles.footerLeft}>
                    <TextRegular text="Pesan Tiket untuk 30 Jan, 2023" color="#34A853" style={styles.textPesan} />
                </View>
                <View style={styles.footerRight}>
                    <TextRegular text="Harga mulai dari" color="#434F65" style={styles.priceStart} />
                    <TextBold text={NumberFormatter(item?.price, 'Rp')} style={styles.titlePrice} />
                </View>
            </View>
        </TouchableOpacity>
        )
    }

    return (
        <ScrollView style={styles.container}>
            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <Header type="main" text="Event" />
            <View style={styles.wrapperSearch}>
                <View style={styles.searchLeft}>
                    <IcSearch stroke="#B6C6E3" width={22} style={{marginRight: scaleSize(8)}} height={22} />
                    <TextInput style={{width: '100%'}} />
                </View>
                <View style={styles.searchRight}>
                    <IcLocation width={20} height={20} style={{marginRight: scaleSize(8)}} />
                    <Text style={styles.allLocation}>Semua lokasi</Text>
                </View>
            </View>

            <View style={styles.wrapperMenu}>   
                <TouchableOpacity activeOpaciity={0.8} onPress={() => setActiveStatus(true)}  style={styles.childMenu}>
                    <IcFilter width={12} height={12} style={{marginRight: scaleSize(4)}} />
                    <Text style={styles.textMenu}>Status</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpaciity={0.8} style={styles.childMenu} onPress={() => setActiveModalFilter(true)}>
                    <IcCalendar width={12} height={12} style={{marginRight: scaleSize(4)}} />
                    <Text style={styles.textMenu}>Tanggal</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpaciity={0.8} style={styles.childMenu} onPress={() => setActiveHarga(true)}>
                    <IcWallet width={12} height={12} style={{marginRight: scaleSize(4)}} />
                    <Text style={styles.textMenu}>Harga</Text>
                </TouchableOpacity>
            </View>

            <TextRegular text="Menampilkan 40 event" style={styles.result} color="#434F65" type="Text Regular 16" />
            <Gap height={4} />
            <FlatList 
                pagingEnabled={false}
                data={EventData}
                renderItem={renderItem}
                ItemSeparatorComponent={<View height={scaleSize(16)} />}
            />
           <Gap height={40} />


           <BottomSheet isVisible={activeModalFilter} height={650} onClose={() => setActiveModalFilter(false)}>
                <View style={styles.wrapperModaStatusTop}>
                    <TextBold text="Tanggal" color="#000000" style={styles.titleModalStatus} />
                    <TouchableOpacity 
                        activeOpacity={0.9} 
                        onPress={() => setActiveModalFilter(false)} 
                        style={styles.btnStatusModal}
                    >
                        <TextRegular color="rgba(67, 79, 101, 1)" text="x" type="Text Regular 12" />
                    </TouchableOpacity>
                </View>
                <Calendar 
                        markingType={'custom'}
                        markedDates= {{
                            '2023-02-21': {customStyles: {
                                container: {
                                    borderColor: 'rgba(26, 148, 255, 1)',
                                    borderRadius: scaleSize(6),
                                    borderWidth: scaleSize(1)
                                }
                            }}
                        }}
                    />
                    <Gap height={4} />
                <View style={styles.wrapperMenuModalButton}>
                    <View style={styles.childBtn}>
                        <FilledButton text="Reset" onPress={() => setActiveModalFilter(false)} 
                        style={{fontWeight: 'bold'}} textColor="rgba(19, 129, 182, 1)" backgroundColor='rgba(19, 129, 182, 0.1)' />
                    </View>
                    <View style={styles.childBtn}>
                        <FilledButton text="Filter" textColor={colors.White} backgroundColor="#1381B6" />
                    </View>
                </View>
            </BottomSheet>

            <BottomSheet isVisible={activeStatus} onClose={() => setActiveStatus(false)}>
                <View style={styles.wrapperModaStatusTop}>
                    <TextBold text="Status" color="#000000" style={styles.titleModalStatus} />
                    <TouchableOpacity 
                        activeOpacity={0.9} 
                        onPress={() => setActiveStatus(false)} 
                        style={styles.btnStatusModal}
                    >
                        <Text style={styles.X}>x</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.menuStatus}>
                    <TextBold text="Sedang Berlangsung" style={styles.textStatus} />
                    <Radio isCheck={activeRadio1} onPress={() => {
                         setActiveRadio2(false),
                         setActiveRadio1(true)
                    }} />
                </View>
                <View style={styles.menuStatus}>
                    <TextBold text="Akan Datang" style={styles.textStatus} />
                    <Radio isCheck={activeRadio2} onPress={() => {
                        setActiveRadio2(true),
                        setActiveRadio1(false)
                    }} />
                </View>
                <View style={styles.wrapperMenuModalButton}>
                    <View style={styles.childBtn}>
                        <FilledButton text="Reset" onPress={() => {
                            setActiveRadio1(false),
                            setActiveRadio2(false)
                        }} 
                        style={{fontWeight: 'bold'}} textColor="rgba(19, 129, 182, 1)" backgroundColor='rgba(19, 129, 182, 0.1)' />
                    </View>
                    <View style={styles.childBtn}>
                        <FilledButton text="Filter" textColor={colors.White} backgroundColor="#1381B6" />
                    </View>
                </View>
            </BottomSheet>

            <BottomSheet isVisible={activeHarga} onClose={() => setActiveHarga(false)}>
                <View style={styles.wrapperModaStatusTop}>
                    <TextBold text="Harga" color="#000000" style={styles.titleModalStatus} />
                    <TouchableOpacity 
                        activeOpacity={0.9} 
                        onPress={() => setActiveHarga(false)} 
                        style={styles.btnStatusModal}
                    >
                        <TextRegular color="rgba(67, 79, 101, 1)" text="x" type="Text Regular 12" />
                    </TouchableOpacity>
                </View>
                <View style={styles.menuStatus}>
                    <TextBold text="Harga Terendah" style={styles.textStatus} />
                    <Radio isCheck={activeRadio1} onPress={() => {
                         setActiveRadioHarga2(false),
                         setActiveRadioHarga1(true)
                    }} />
                </View>
                <View style={styles.menuStatus}>
                    <TextBold text="Harga Tertinggi" style={styles.textStatus} />
                    <Radio isCheck={activeRadio2} onPress={() => {
                        setActiveRadioHarga2(true),
                        setActiveRadioHarga1(false)
                    }} />
                </View>
                <View style={styles.wrapperMenuModalButton}>
                    <View style={styles.childBtn}>
                        <FilledButton text="Reset" onPress={() => {
                            setActiveRadioHarga1(false),
                            setActiveRadioHarga2(false)
                        }} 
                        style={{fontWeight: 'bold'}} textColor="rgba(19, 129, 182, 1)" backgroundColor='rgba(19, 129, 182, 0.1)' />
                    </View>
                    <View style={styles.childBtn}>
                        <FilledButton text="Filter" textColor={colors.White} backgroundColor="#1381B6" />
                    </View>
                </View>
            </BottomSheet>
            <Gap height={70} />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    searchRight: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    wrapperSearch: {
        borderRadius: scaleSize(99),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 1,
        marginTop: scaleSize(8),
        marginHorizontal: scaleSize(22),
        borderColor: '#B6C6E3',
        paddingVertical: scaleSize(2),
        paddingHorizontal: scaleSize(20),
    },
    searchLeft: {
        width: '60%',
        alignItems: 'center',
        flexDirection: 'row'
    },
    searchRight: {
        height: '80%',
        borderLeftColor: '#EBEDF1',
        paddingLeft: scaleSize(8),
        borderLeftWidth: scaleSize(2),
        width: '40%',
        alignItems: 'flex-end',
        flexDirection: 'row',
        alignItems: 'center',
        flexDirection: 'row'
    },
    allLocation: {
        color: '#1381B6',
        fontWeight: 500,
        fontSize: scaleSize(14)
    },
    wrapperMenu: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: scaleSize(26),
        borderBottomColor: '#B6C6E3',
        borderBottomWidth: scaleSize(1),
        paddingBottom: scaleSize(16),
        justifyContent: 'space-between',
        paddingHorizontal: scaleSize(24)
    },
    childMenu: {
        width: '33%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textMenu: {
        color: '#434F65',
        fontSize: scaleSize(12),
    },
    cardMain: {
        marginHorizontal: scaleSize(20),
        maxHeight: scaleSize(260),
        overflow: 'hidden',
        borderWidth: scaleSize(1),
        borderColor: colors.White,
        elevation: scaleSize(1),
        backgroundColor: colors.White,
        borderRadius: scaleSize(20),
        padding: scaleSize(14)
    },
    cardMainTop: {
        width: '100%',
        height: '55%',
        marginTop: scaleSize(-4),
        borderRadius: scaleSize(10)
    },
    cardMainBottom: {
        height: '45%'
    },
    result: {
        marginLeft: scaleSize(20),
        marginVertical: scaleSize(10),
        fontSize: scaleSize(14)
    },
    titleCard: {
        color: colors.Black,
        fontSize: scaleSize(18),
        fontWeight: 'bold',
        marginTop: scaleSize(4),
        marginVertical: scaleSize(12)
    },
    priceStart: {
        color: 'rgba(158, 157, 159, 1)',
        fontSize: scaleSize(10),
        marginBottom: scaleSize(2)
    },
    textPesan: {
        fontSize: scaleSize(12),
        width: '70%'
    },
    textDate: {
        fontSize: scaleSize(10),
        marginTop: scaleSize(-12),
        color: 'rgba(67, 79, 101, 1)',
        marginBottom: scaleSize(20)
    },
    titlePrice: {
        color: 'red',
        fontWeight: 'bold'
    },
    footerCardMain: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: scaleSize(-40),
        height: 'auto',
        justifyContent: 'space-between',
    },
    footerLeft: {
        width: '50%',
    },
    footerRight: {
        width: '50%',
        flexDirection: 'column',
        alignItems: 'flex-end'
    },
    wrapperModaStatusTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: scaleSize(20),
        marginTop: scaleSize(24),
        marginBottom: scaleSize(14)
    },
    btnStatusModal: {
        width: 24,
        height: 24,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: scaleSize(1),
        borderRadius: scaleSize(6),
    },
    X: {
        fontSize: scaleSize(14),
        top: scaleSize(-2)
    },
    menuStatus: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: scaleSize(20),
        marginVertical: scaleSize(20)
    },
    wrapperMenuModalButton: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        marginBottom: scaleSize(20),
        marginTop: scaleSize(10),
        justifyContent: 'space-between'
    },
    childBtn: {
        paddingHorizontal: scaleSize(20),
        width: '50%'
    },
    titleModalStatus: {
        fontWeight: 'bold',
        fontSize: scaleSize(20),
    },
    textStatus: {
        fontWeight: 600,
        color: colors.Black
    }
});

export default Event;