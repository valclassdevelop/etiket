import React, { useState } from 'react';
import { ScrollView, StatusBar, StyleSheet, View } from 'react-native';
import { ScreenStackHeaderLeftView } from 'react-native-screens';
import { Divider, FilledButton, Gap, Header, TextInput } from '../../components';
import { colors, scaleSize } from '../../utils';

const UpdatePassword = ({navigation}) => {

    const [activeHide1, setActiveHide1] = useState(true);
    const [activeHide2, setActiveHide2] = useState(true);
    const [activeHide3, setActiveHide3] = useState(true);

    return (
        <View style={styles.container}>
            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <View>
                <Header text="Update Password" onBack={() => navigation.goBack(-1)} />
                <Gap height={30} />
                <View style={styles.wrapperForm}>
                    <TextInput 
                        type="password" 
                        label="Password Lama" 
                        placeholder="Password Lama" 
                        onPress={() => setActiveHide1(!activeHide1)} hide={activeHide1} 
                    />
                    <Gap height={20} />
                    <TextInput 
                        type="password" 
                        label="Password Baru" 
                        placeholder="Password Baru" 
                        onPress={() => setActiveHide2(!activeHide2)} hide={activeHide2} 
                    />
                    <Gap height={20} />
                    <TextInput 
                        type="password" 
                        label="Konfirmasi Password Baru" 
                        placeholder="Konfirmasi Password" 
                        onPress={() => setActiveHide3(!activeHide3)} hide={activeHide3} 
                    />
                    <Gap height={20} />
                </View>
            </View>
            <View>
                <Divider height={2} />
                <View style={styles.wrapperBtn}>
                    <FilledButton text="Simpan" backgroundColor='rgba(19, 129, 182, 1)' textColor={colors.White} />
                </View>
            <Divider height={20} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White,
        justifyContent: 'space-between',
    },
    wrapperForm: {
        marginHorizontal: scaleSize(20)
    },
    wrapperBtn: {
        marginHorizontal: scaleSize(20)
    }
});

export default UpdatePassword;