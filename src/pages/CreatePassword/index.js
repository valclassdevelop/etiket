import React, { useState } from "react";
import { Dimensions, Image, ScrollView, StatusBar, StyleSheet, Text, View } from "react-native";
import { Keys, Logo } from "../../assets/Dummy";
import { Gap, Header, TextBold, TextInput, TextRegular } from "../../components";
import { FilledButton } from "../../components/Button";
import { colors } from "../../utils";
import { BottomSheetCenter } from "../../components/BottomSheet";


const CreatePassword = () => {
    const [active, setActive] = useState(false);

    return (
        <ScrollView style={styles.container}>
        <StatusBar
            barStyle="light-content"
            backgroundColor={colors.primary}
            />
        <Header />
        <Image 
            source={Logo}
            alt="Logo aplikasi"
            style={{
                marginLeft: 20,
                width: 200,
                marginTop: -40,
                maxHeight: 200,
            }}
            resizeMode="contain"
        />
        <View style={styles.wrapperTitle}>
            <TextBold color={colors.Black} text="Buat Password" type="Title Bold 28" style={styles.titleLogin} />
            <TextRegular style={styles.pLogin} type="Text Reguler 16" color="#434F65" text="Create your new password" />
        </View>

        <View style={styles.wrapperInput}>
            <TextInput label="Password Baru" placeholder="Password Baru" />
            <Gap height={20} />
            <TextInput label="Konfirmasi Password Baru" placeholder="Kofirmasi Password" />
            <Gap height={20} />
        </View>
        
        <View style={styles.wrapperBtnLogin}>
            <FilledButton style={styles.btnLogin} text="Masuk" />
        </View>

        <BottomSheetCenter isVisibleCenter={active} onClose={() => setActive(false)}>
            <View style={styles.wrapperModal}>
                <Image 
                    source={Keys} 
                    alt="icon modal" 
                    style={{width: 160, maxHeight: 160}}
                />
                <TextRegular text="Selamat!" style={styles.CongrtsText} />
                <TextRegular text="Password akun kamu telah berhasil diganti." style={styles.textPCongrats} />

                <View style={styles.wrapperBtn}>
                    <FilledButton style={styles.btnLogin} text="Back to Sign in" />
                </View>
            </View>
        </BottomSheetCenter>

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White,
    },
    wrapperBtn: {
        width: '80%',
        marginTop: 120
    },
    CongrtsText: {
        fontSize: 24,
        fontWeight: 'bold',
        marginTop: 20,
        lineHeight: 32,
        color: '#1381B6'
    },
    textPCongrats: {
        width: '90%',
        textAlign: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
        top: 15
    },
    loading: {
        top: 80,
    },
    wrapperModal: {
        flexDirection: 'column',
        paddingTop: 40,
        alignItems: 'center'
    },
    textCreateNew: {
        marginLeft: 4,
        fontWeight: 'bold',
        color: '#1381B6'
    },
    wrapperBtnLogin: {
        marginTop: 40,
        marginHorizontal: 20
    },
    wrapperInput: {
        marginTop: 32,
        marginHorizontal: 20
    },
    titleLogin: {
        fontWeight: 'bold',
        marginLeft: 20,
        marginBottom: 6,
    },
    pLogin: {
        width: '90%',
        marginLeft: 20,
    },
    wrapperTitle: {
        marginTop: -30,
    }
});

export default CreatePassword;