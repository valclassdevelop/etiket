import React, { useState } from 'react';
import { FlatList, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { IcChevronRight, IcLocation } from '../../assets';
import { Banner } from '../../assets/Dummy';
import { BottomSheet, Divider, FilledButton, Gap, Header, TextBold, TextInput, TextMedium, TextRegular } from '../../components';
import Radio from '../../components/Radio';
import { colors, NumberFormatter, scaleSize } from '../../utils';

const Checkout = ({navigation}) => {

    const [activeRadio1, setActiveRadio1] = useState(false);
    const [activeRadio2, setActiveRadio2] = useState(false);
    const [activeRadio3, setActiveRadio3] = useState(false);
    const [activeKonfirmasi, setActiveKonfirmasi] = useState(false);

    const dataCheck = [
        {text: 'Konfirmasi instan'},
        {text: 'Tidak perlu reservasi'},
        {text: 'Berlaku ditanggal yang terpilih'},
        {text: 'Tidak bisa refund'},
    ];

    const renderItem = ({item}) => {
        return (
            <View style={{flexDirection: 'row', flexWrap: 'wrap', width: 'auto', alignItems: 'center'}}>
                <IcChevronRight width={12} height={12} fill="#1381B6" />
                <Gap width={10} />
                <TextRegular type="Text Regular 10" text={item?.text} />
            </View>
        )
    }

    return (
        <ScrollView style={styles.container}>
            <Header text="Selesaikan Pesanan" onBack={() => navigation.goBack(-1)}  />
            <View style={styles.cardCheckout}>
                <View style={styles.cardTop}>
                    <View style={styles.imgCheckout}>
                        <Image source={Banner} alt="img-product" style={{width: '100%', maxHeight: '100%'}} resizeMode="cover" />
                    </View>
                    <Gap width={10} />
                    <TextBold color={colors.Black} text="National Music Festival" type="Title Bold 16" />
                    <IcChevronRight stroke={colors.Black} width={12} height={12} style={{marginLeft: 'auto'}} />
                </View>
                <Gap height={10} />
                <Divider height={1} />
                <Gap height={10} />
                    <TextBold text="Yellow" color={colors.Black} type="Text Bold 14" />
                <Gap height={10} />
                <View style={styles.pax}>
                    <TextRegular text="1 TIket" color={colors.Black} type="Text Bold 14" />
                    <Text style={{marginHorizontal: scaleSize(4)}}>-</Text>
                    <TextRegular text="1 Pax" color="#434F65" type="Text Regular 14" />
                </View>
                <Gap height={10} />
                <Divider height={1} />
                <Gap height={10} />
                <TextRegular text="Tanggal DIpilih" color="#434F65" type="Text Regular 10" />
                <Gap height={10} />
                <TextMedium text="Senin, 30 January 2023" color={colors.Black} type="Text Medium 14" />
                <Gap height={10} />
                <Divider height={1} />
                <Gap height={10} />
                <FlatList 
                    data={dataCheck}
                    ItemSeparatorComponent={() => <View height={10} />}
                    renderItem={renderItem}
                />
            </View>
            <Gap height={20} />
            <Divider height={4} backgroundColor="#EBEDF1" />
            <Text style={styles.titleDetail2}>Detail Pemesan</Text>
            <Gap height={10} />
            <TextRegular style={{marginLeft: scaleSize(20)}} text="Isi formulir ini dengan benar karena e-tiket akan dikirim ke alamat email sesuai data pemesanan." type="Text Regular 10" color="#434F65" />
            <Gap height={24} />
            <View style={styles.wrapperNick}> 
                <TouchableOpacity activeOpactiy={0.8} onPress={() => {
                    setActiveRadio1(true);
                    setActiveRadio2(false);
                    setActiveRadio3(false);
                }} style={styles.childNick}>
                    <Radio isCheck={activeRadio1} />
                    <Gap  width={12}/>
                    <TextRegular text="Tuan" type="Text Regular 14" />
                </TouchableOpacity>
                <TouchableOpacity activeOpactiy={0.8} onPress={() => {
                    setActiveRadio1(false);
                    setActiveRadio2(true);
                    setActiveRadio3(false);
                }} style={styles.childNick}>
                    <Radio isCheck={activeRadio2} />
                    <Gap width={12} />
                    <TextRegular text="Nyonya" type="Text Regular 14" />
                </TouchableOpacity>
                <TouchableOpacity activeOpactiy={0.8} onPress={() => {
                    setActiveRadio1(false);
                    setActiveRadio2(false);
                    setActiveRadio3(true);
                }} style={styles.childNick}>
                    <Radio isCheck={activeRadio3} />
                    <Gap width={12} />
                    <TextRegular text="Nona" type="Text Regular 14" />
                </TouchableOpacity>
            </View>
            <Gap height={20} />
            <View style={styles.formCheckout}>
                <TextInput label="Nama lengkap" />
                <Gap height={8} />
                <View style={styles.wrapperPhone}>
                    <View style={styles.kode}>
                        <TextRegular text="Kode" color="#000" type="Text Regular 14"></TextRegular>
                        <Gap height={4} />
                        <View style={styles.phoneLeft}>
                            <View style={styles.number}>
                                <TextRegular text="+62" type="Text Regular 14" />
                            </View>
                        </View>
                    </View>
                    <Gap width={10} />
                    <View style={styles.inputNumber}>
                        <TextInput type="phone" label="Nomor Telepon" borderColor="rgba(182, 198, 227, 1)" />
                    </View>
                </View>
                <Gap height={8} />
                <TextInput label="Email" />
            </View>
            <Gap height={40} />
            <View style={styles.footerDetailEvent}>
                <View style={styles.fdeLeft}>
                    <TextRegular type="Text Regular 14" color="#434F65" text="Total" />
                    <Gap height={4} />
                    <TextBold text={NumberFormatter(1500000, 'IDR ')} type="Text Bold 18" color="red" />
                </View>
                <Gap height={16} />
                <FilledButton text="Lanjutkan Pembayaran" onPress={() => setActiveKonfirmasi(true)} backgroundColor='#1381B6' textColor={colors.White} />
            </View>


            <BottomSheet isVisible={activeKonfirmasi} height={700} onClose={() => setActiveKonfirmasi(false)}>
                <View style={styles.wrapperModaStatusTop}>
                    <TextBold text="Konfirmasi Pembayaran" color="#000000" style={styles.titleModalStatus} />
                    <TouchableOpacity 
                        activeOpacity={0.9} 
                        onPress={() => setActiveKonfirmasi(false)} 
                        style={styles.btnStatusModal}
                    >
                        <Text style={styles.X}>x</Text>
                    </TouchableOpacity>
                </View>
                <Gap height={12} />
                <Divider height={1} />
                <Gap height={12} />
                <TextBold text="Tiket" color={colors.Black} type="Text Bold 14" style={{marginLeft: scaleSize(20)}} />
                <Gap height={16} />
                <View style={styles.cardTop2}>
                    <View style={styles.imgCheckout}>
                        <Image source={Banner} alt="img-product" style={{width: '100%', maxHeight: '100%'}} resizeMode="cover" />
                    </View>
                    <Gap width={10} />
                    <View style={styles.cardTop3}>
                        <TextBold color={colors.Black} text="National Music Festival" type="Title Bold 14" />
                        <Gap height={2} />
                        <TextRegular text="Mon, Dec 23 - 18.00 - 23.00 PM" color="#115888" type="Text Regular 10" />
                        <Gap height={8} />
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <IcLocation width={8} height={8} />
                            <Gap width={8} />
                            <TextRegular text="Jakarta, Indonesia" color="#434F65" type="Text Regular 10" />
                        </View>
                    </View>
                </View>
                <Gap height={16} />
                <TextBold text="Detail Pembayaran" color={colors.Black} type="Text Bold 14" style={{marginLeft: scaleSize(20)}} />
                <Gap height={20} />
                <View style={styles.childFooter}>
                    <TextRegular text="Nama Pesanan" color="#434F65" type="Text Regular 14"/>
                    <TextBold text="Shohei" color={colors.Black} type="Text Bold 14" />
                </View>
                <View style={styles.childFooter}>
                    <TextRegular text="Jumah Tiket" color="#434F65" type="Text Regular 14" />
                    <TextBold text="1 Tiket" color={colors.Black} type="Text Bold 14"d />
                </View>
                <View style={styles.childFooter}>
                    <TextRegular text="Harga" color="#434F65" type="Text Regular 14" />
                    <TextBold text={NumberFormatter(1500000, 'IDR ')} color={colors.Black} type="Text Bold 14" />
                </View>
                <View style={styles.childFooter}>
                    <TextRegular text="Pajak" color="#434F65" type="Text Regular 14" />
                    <TextBold text={NumberFormatter(5000, 'IDR ')} color={colors.Black} type="Text Bold 14" />
                </View>
                <Gap height={16} />
                <Divider height={1} />
                <Gap height={16} />
                <View style={styles.wrapperMenuModalButton}>
                    <View style={styles.childBtn}>
                        <FilledButton text="Cancel" onPress={() => setActiveKonfirmasi(false)} 
                        style={{fontWeight: 'bold'}} textColor="rgba(19, 129, 182, 1)" backgroundColor='rgba(19, 129, 182, 0.1)' />
                    </View>
                    <View style={styles.childBtn}>
                        <FilledButton text="Bayar" textColor={colors.White}onPress={() => navigation.navigate('ContinuePayment1')} backgroundColor="#1381B6" />
                    </View>
                </View>
            </BottomSheet>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    cardCheckout: {
        height: 'auto',
        borderRadius: scaleSize(10),
        padding: scaleSize(20),
        marginTop: scaleSize(20),
        marginHorizontal: scaleSize(20),
        backgroundColor: colors.White,
        elevation: scaleSize(3)
    },
    titleDetail2: {
        fontSize: scaleSize(20),
        fontWeight: 'bold',
        marginTop: scaleSize(20),
        color: colors.Black,
        marginLeft: scaleSize(20)
    },
    cardTop2: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: scaleSize(20),
        height: scaleSize(80),
    },
    cardTop: {
        flexDirection: 'row',
        alignItems: 'center',
        height: scaleSize(80),
    },
    imgCheckout: {
        width: scaleSize(80),
        height: '100%',
        borderRadius: scaleSize(10),
        overflow: 'hidden'
    },
    pax: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    wrapperNick: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
        justifyContent: 'space-between',
    },
    childNick: {
        width: '33%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    formCheckout: {
        marginHorizontal: scaleSize(18),
    },
    wrapperPhone: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
    },
    phoneLeft: {
        borderWidth: scaleSize(1),
        borderColor : '#B6C6E3',
        borderRadius: scaleSize(8),
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: scaleSize(24),
        paddingVertical: scaleSize(14)
    },
    kode: {
        width: 'auto',
    },
    inputNumber: {
        flex: 1
    },
    footerDetailEvent: {
        backgroundColor: colors.White,
        borderTopColor: '#EBEDF1',
        borderTopWidth: scaleSize(2),
        paddingHorizontal: scaleSize(20),
        paddingVertical: scaleSize(14),
        flexDirection: 'column',
    },
    fdeLeft: {
        width: '60%'
    },
    btnfde: {
        width: scaleSize(110),
        paddingVertical: scaleSize(8),
        backgroundColor: '#1381B6',
        color: colors.White,
        borderRadius: scaleSize(4),
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3
    },
    wrapperModaStatusTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: scaleSize(20),
        marginTop: scaleSize(24),
        marginBottom: scaleSize(14)
    },
    btnStatusModal: {
        width: 24,
        height: 24,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: scaleSize(1),
        borderRadius: scaleSize(6),
    },
    X: {
        fontSize: scaleSize(14),
        top: scaleSize(-2)
    },
    childFooter: {
        marginHorizontal: scaleSize(20),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: scaleSize(8),
    },
    wrapperMenuModalButton: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        marginBottom: scaleSize(20),
        marginTop: scaleSize(10),
        justifyContent: 'space-between'
    },
    childBtn: {
        paddingHorizontal: scaleSize(20),
        width: '50%'
    },
});

export default Checkout;