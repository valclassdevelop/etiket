import React from 'react';
import { Image, ScrollView, StatusBar, StyleSheet, View } from 'react-native';
import { User } from '../../assets/Dummy';
import { FilledButton, Gap, Header, TextInput } from '../../components';
import { colors, scaleSize } from '../../utils';

const EditProfile = ({navigation}) => {
    return (
        <ScrollView style={styles.container}>
            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <Header text="Edit Profil" onBack={() => navigation.goBack(-1)} />
            <Gap height={24} />
            <View style={styles.wrapperTop}>
                <View style={styles.wrapperImg}>
                    <Image source={User} alt="User-photo" resizeMode="cover" style={styles.imgUser} />
                </View>
                <Gap height={20} />
                <View style={styles.btnTop}>
                    <FilledButton text="Ganti Foto" backgroundColor='rgba(19, 129, 182, 0.1)' textColor='rgba(19, 129, 182, 1)' />
                </View>
            </View>
            <Gap height={30} />
            <View style={styles.wrapperForm}>
                <TextInput label="Nama" />
            </View>
            <Gap height={20} />
            <View style={styles.wrapperForm}>
                <TextInput label="Nomor Telepone" />
            </View>
            <Gap height={20} />
            <View style={styles.wrapperForm}>
                <TextInput label="Email" />
            </View>
            <Gap height={20} />
            <View style={styles.wrapperForm}>
                <TextInput label="Tanggal Lahir" />
            </View>
            <Gap height={20} />
            <View style={styles.wrapperForm}>
                <TextInput type="textarea" label="Alamat" />
            </View>
            <Gap height={100} />
            <View style={styles.btnBottom}>
                <FilledButton text="Simpan" backgroundColor='rgba(19, 129, 182, 1)' textColor={colors.White} />
            </View>
            <Gap height={20} />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White
    },
    wrapperImg: {
        width: scaleSize(100),
        height: scaleSize(100),
        borderRadius: scaleSize(100),
        overflow: 'hidden',
    },
    wrapperTop: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgUser: {
        width: '100%',
        maxHeight: '100%'
    },  
    btnTop: {
        width: '35%',
    },
    wrapperForm: {
        marginHorizontal: scaleSize(20)
    },
    btnBottom: {
        marginHorizontal: scaleSize(20)
    }
});

export default EditProfile;