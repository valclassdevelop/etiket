import React from 'react';
import { ScrollView, StyleSheet, View, Image, StatusBar } from 'react-native';
import { IcCopy } from '../../assets';
import { Barcode } from '../../assets/Dummy';
import { FilledButton, Gap, Header, TextBold, TextRegular } from '../../components';
import { colors, NumberFormatter, scaleSize } from '../../utils';

const Etiket = ({navigation}) => {
    return (
        <ScrollView style={styles.container}>
            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <Header text="E-ticket" onBack={() => navigation.goBack(-1)} />
            <Gap height={20} />
            <View style={styles.imgBarcode}>
                <Image source={Barcode} alt="barcode" style={{width: 280, maxHeight: 300}} resizeMode="contain" />
            </View>
            <Gap height={20} />
            <View style={styles.cardOne}>
                <View style={styles.childCardOne}>
                    <TextRegular text="Event" type="Text Regular 10" />
                    <TextBold text="National Music Festival" color={colors.Black} type="Text Bold 10" />
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text="Tanggal $ Waktu" type="Text Regular 10" />
                    <TextBold text="30 Januari 2023 . 07.17 WIB" color={colors.Black} type="Text Bold 10" />
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text=" Lokasi Event" type="Text Regular 10" />
                    <TextBold text="Jakarta, Indonesia" color={colors.Black} type="Text Bold 10" />
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text="Event Organizer" type="Text Regular 10" />
                    <TextBold text="National Music" color={colors.Black} type="Text Bold 10" />
                </View>
            </View>
            <Gap height={20} />
            <View style={styles.cardOne}>
                <View style={styles.childCardOne}>
                    <TextRegular text="Nama" type="Text Regular 10" />
                    <TextBold text="Shohei" color={colors.Black} type="Text Bold 10" />
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text="Jenis Kelamin" type="Text Regular 10" />
                    <TextBold text="Pria" color={colors.Black} type="Text Bold 10" />
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text="Tanggal Lahir" type="Text Regular 10" />
                    <TextBold text="17 - 07 - 1997" color={colors.Black} type="Text Bold 10" />
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text="Nomor Telepon" type="Text Regular 10" />
                    <TextBold text="098 178 126 172" color={colors.Black} type="Text Bold 10" />
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text="Email" type="Text Regular 10" />
                    <TextBold text="shohei@adreess.com" color={colors.Black} type="Text Bold 10" />
                </View>
            </View>
            <Gap height={20} />
            <View style={styles.cardOne}>
                <View style={styles.childCardOne}>
                    <TextRegular text="1 Tiket" type="Text Regular 10" />
                    <TextBold text={NumberFormatter(1500000, 'IDR ')} color={colors.Black} type="Text Bold 10" />
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text="Pajak" type="Text Regular 10" />
                    <TextBold text={NumberFormatter(5000, 'IDR ')} color={colors.Black} type="Text Bold 10" />
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text="Total" type="Text Regular 10" />
                    <Gap height={20} />
                    <TextBold text={NumberFormatter(1505000, 'IDR ')} color={colors.Black} type="Text Bold 10" />
                </View>
            </View>
            <Gap height={20} />
            <View style={styles.cardOne}>
                <View style={styles.childCardOne}>
                    <TextRegular text="Metode Pembayaran" type="Text Regular 10" />
                    <TextBold text="MasterCard" color={colors.Black} type="Text Bold 10" />
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text="ID Pesanan" type="Text Regular 10" />
                    <View style={{flexDirection: 'row'}}>
                        <TextBold text="#0891278" color={colors.Black} type="Text Bold 10" />
                        <Gap width={4} />
                        <IcCopy width={10} height={10} stroke="rgba(19, 129, 182, 1)" />
                    </View>
                </View>
                <View style={styles.childCardOne}>
                    <TextRegular text="Sttaus" type="Text Regular 10" />
                    <View style={styles.status}>
                        <TextRegular text="Lunas" color="rgba(52, 168, 83, 1)" type="Text Regular 10" />
                    </View>
                </View>
            </View>
            <Gap height={24} />
            <View style={{marginHorizontal: scaleSize(20)}}>
                <FilledButton text="Download Tiket" textColor={colors.White} backgroundColor="rgba(19, 129, 182, 1)" />
            </View>
            <Gap height={24} />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White    
    } ,
    imgBarcode: {
        marginLeft: 'auto',
        marginRight: 'auto',
        overflow: 'hidden',
        height: scaleSize(280)
    },
    cardOne: {
        borderRadius: scaleSize(10),
        backgroundColor: colors.White,
        padding: scaleSize(14),
        elevation: 3,
        height: 'auto',
        marginHorizontal: scaleSize(20)
    },
    childCardOne: {
        marginBottom: scaleSize(20),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    status: {
        borderRadius: scaleSize(12),
        borderWidth: scaleSize(1),
        borderColor: 'rgba(52, 168, 83, 1)',
        paddingHorizontal: scaleSize(10),
        paddingVertical: scaleSize(4),
    }
});

export default Etiket;