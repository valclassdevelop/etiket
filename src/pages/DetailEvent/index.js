import React, { useState } from 'react';
import { Dimensions, FlatList, Image, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { IcAngelRight, IcArrowLeft, IcCalendar, IcChevronRight, IcCupon, IcLocation, IcLocationBlack, IcMessage, IcMessageBlue } from '../../assets';
import { BannerDetail, Peta } from '../../assets/Dummy';
import DataPenukaran from '../../assets/JSON/DataPenukaran';
import DataSyarat from '../../assets/JSON/DataSyarat';
import { BottomSheet, Divider, Gap, TextBold, TextRegular } from '../../components';
import { colors, NumberFormatter, scaleSize } from '../../utils';

const DetailEvent = ({navigation}) => {
    const [activePenukaran, setActivePenukaran] = useState(false);
    const [activeSyarat, setActiveSyarat] = useState(false);

    const back = (navigation) => {
        // navigation.navigate('Event');
    }

    const dataList = [
        {description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'},
        {description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'},
        {description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'},
    ]

    const dataPaket = [
        {
            title: 'Yellow',
            desc: 'Harga tiket sudah termasuk pajak dan biaya pemrosesan tiket sebesar Rp 15.000,-',
            price: 1500000
        },
        {
            title: 'Green',
            desc: 'Harga tiket sudah termasuk pajak dan biaya pemrosesan tiket sebesar Rp 15.000,-',
            price: 1500000
        },
        {
            title: 'Blue',
            desc: 'Harga tiket sudah termasuk pajak dan biaya pemrosesan tiket sebesar Rp 15.000,-',
            price: 1500000
        },
    ]

    const renderPaket = ({item}) => {
        return (
            <TouchableOpacity activeOpacity={0.9} style={styles.cardPaket}>
                <TextBold text={item?.title} color={colors.Black} type="Text Bold 14" />
                <Gap height={4} />
                <Text style={styles.descPaket}>{item?.desc}</Text>
                <Gap height={4} />
                <Gap height={12} />
                <View style={{width: '90%'}}>
                    <Divider height={2} />
                </View>
                <Gap height={12} />
                <View style={styles.footerPaket}>
                    <Text style={styles.priceStart}>{NumberFormatter(item?.price, 'IDR ')}</Text>
                    <TouchableOpacity activeOpacity={0.8} style={styles.btnPaket} onPress={() => navigation.navigate('DetailOrder')}>
                        <Text style={{color: colors.White, fontSize: scaleSize(14)}}>Pilih paket</Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <ScrollView style={styles.container}>
              <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <View style={styles.heroDetail}>
                <View style={styles.wrapperTop}>
                    <TouchableOpacity activeOpacity={0.94} style={styles.btnLeft} onPress={() => navigation.goBack(-1)}>
                        <IcArrowLeft width={15} height={15} stroke="#000" />
                    </TouchableOpacity>
                    <IcMessage width={20} height={20} style={{elevation: 1}} />
                </View>
                <Image resizeMode='cover' style={{width: '100%', maxHeight: '100%', opacity: 0.9}} source={BannerDetail} alt="banner" />
            </View>
            <Gap height={12} />
            <TextRegular style={styles.loc} text="Event > Jakarta, Indonesia" />
            <Gap height={8} />
            <Text style={styles.titleDetail}>National Music Festival</Text>
            <Gap height={12} />
            <View style={{width: '90%',  marginLeft: 'auto', marginRight: 'auto',}}>
                <Divider height={2} backgroundColor="#EBEDF1" />
            </View>
            <View style={styles.date}>
                <IcCalendar height={12} width={12} />
                <Text style={styles.textDate}>30 Januari 2023</Text>
            </View>
            <Gap height={24} />
            <Divider height={4} backgroundColor="#EBEDF1" />
            <Text style={styles.titleDetail2}>Highlight</Text>
            <View style={styles.wrapperHighlight}>
                <FlatList
                    data={dataList}
                    renderItem={({item}) => <Text style={{ fontSize: 14, color: '#434F65', marginBottom: scaleSize(20) }}>{`\u2022 ${item?.description}`}</Text>}
                />
            </View>
            <Gap height={24} />
            <Divider height={4} backgroundColor="#EBEDF1" />
            <Text style={styles.titleDetail2}>Paket</Text>
            <View style={styles.wrapperHighlight}>
                <FlatList
                    data={dataPaket}
                    renderItem={renderPaket}
                />
            </View>
            <Divider height={4} backgroundColor="#EBEDF1" />
            <View style={styles.wrapperInfo}>
                <Text style={styles.titleDetail2}>Info Lainnya</Text>
                <TouchableOpacity activeOpacity={0.8} style={styles.childInfo} onPress={() => setActivePenukaran(true)}>
                    <View activeOpacity={0.8} style={styles.leftInfo}>
                        <IcCupon width={16} height={16} />
                        <Text style={styles.textInfo}>Penukaran Tiket</Text>
                    </View>
                    <View activeOpacity={0.8}>
                        <IcAngelRight width={14} height={14} stroke={colors.Black} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={styles.childInfo} onPress={() => setActiveSyarat(true)}>
                    <View activeOpacity={0.8} style={styles.leftInfo}>
                        <IcCupon width={16} height={16} />
                        <Text style={styles.textInfo}>Syarat & Ketentuan</Text>
                    </View>
                    <View activeOpacity={0.8}>
                        <IcAngelRight width={14} height={14} stroke={colors.Black} />
                    </View>
                </TouchableOpacity>
                <Gap height={10} />
            </View>
            <Divider height={4} backgroundColor="#EBEDF1" />
            <Text style={styles.titleDetail2}>Lokasi</Text>
            <Gap height={14} />
            <View style={styles.peta}>
                <Image source={Peta} alt="peta" style={{width: '100%', maxHeight: '100%'}} resizeMode="cover" />
            </View>
            <Gap height={14} />
            <View style={styles.wrapperLocation}>
                <IcLocationBlack width={12} height={12} style={{marginRight: scaleSize(10)}} />
                <Text style={styles.textLoc}>Jakarta, Indonesia</Text>
            </View>
            <Gap height={20} />
            <View style={styles.wrapperPetunjuk}>
                <View style={styles.childPetunjuk}>
                    <View style={styles.bullet}>
                        <IcMessageBlue width={24} height={24} />
                    </View>
                    <Gap height={6} />
                    <Text style={{fontWeight: 500, color: '#434F65'}}>Petunjuk Arah</Text>
                </View>
                <View style={styles.childPetunjuk}>
                    <View style={styles.bullet}>
                        <IcLocation width={24} height={24} />
                    </View>
                    <Gap height={6} />
                    <Text style={{fontWeight: 500, color: '#434F65'}}>Panduan ke Lokasi</Text>
                </View>
            </View>
            <Divider height={4} backgroundColor="#EBEDF1" />
            <Text style={styles.titleDetail2}>Paket</Text>
            <Gap height={6} />
            <Text style={styles.descFooter}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                sed do eiusmod tempor incididunt ut labore et 
                dolore magna aliqua.
                
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                sed do eiusmod tempor incididunt ut labore et 
                dolore magna aliqua.
            </Text>
            <View style={styles.footerDetailEvent}>
                <View style={styles.fdeLeft}>
                    <TextRegular text="Mulai dari" style={styles.start} />
                    <TextRegular text={NumberFormatter(1500000, 'IDR ')} style={styles.textDiscountPrice} />
                </View>
                <TouchableOpacity activeOpacity={0.8} style={styles.btnfde}>
                    <Text style={styles.textBtnfde}>Pilih Tiket</Text>
                </TouchableOpacity>
            </View>


            {/* Modal penukaran */}
            <BottomSheet isVisible={activePenukaran} height={800} onClose={() => setActivePenukaran(false)}>
                <View style={styles.wrapperModaStatusTop}>
                    <TextBold text="Penukaran Tiket" color="#000000" style={styles.titleModalStatus} />
                    <TouchableOpacity 
                        activeOpacity={0.9} 
                        onPress={() => setActivePenukaran(false)} 
                        style={styles.btnStatusModal}
                    >
                        <Text style={styles.X}>x</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.wrapperPenukaran}>
                    <FlatList
                        data={DataPenukaran}
                        renderItem={({item}) => <Text style={{ fontSize: 14, color: '#434F65', marginBottom: scaleSize(20) }}>{`\u2022 ${item?.description}`}</Text>}
                    />
                </View>
            </BottomSheet>

            {/* Modal persyaratan */}
            <BottomSheet isVisible={activeSyarat} height={960} onClose={() => setActiveSyarat(false)}>
                <View style={styles.wrapperModaStatusTop}>
                    <TextBold text="Syarat dan Ketentuan" color="#000000" style={styles.titleModalStatus} />
                    <TouchableOpacity 
                        activeOpacity={0.9} 
                        onPress={() => setActiveSyarat(false)} 
                        style={styles.btnStatusModal}
                    >
                        <Text style={styles.X}>x</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.wrapperPenukaran}>
                    <Gap height={10} />
                    <Text style={styles.titleSyarat}>Umum</Text>
                    <Gap height={10} />
                    <FlatList
                        data={DataSyarat}
                        renderItem={({item}) => <Text style={{ fontSize: 14, color: '#434F65', marginBottom: scaleSize(20) }}>{`\u2022 ${item?.description}`}</Text>}
                    />
                </View>
            </BottomSheet>

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    heroDetail: {
        height: scaleSize(300)
    },
    wrapperTop: {
        position: 'absolute',
        top: scaleSize(22),
        marginHorizontal: scaleSize(20),
        flexDirection: 'row',
        alignItems: 'center',
        width: Dimensions.get('window').width * 0.88,
        justifyContent: 'space-between',
        zIndex: scaleSize(2)
    },
    btnLeft: {
        backgroundColor: colors.White,
        borderRadius: scaleSize(4),
        width: scaleSize(20),
        height: scaleSize(20),
        alignItems: 'center',
        justifyContent: 'center',
    },
    loc: {
        marginLeft: scaleSize(20),
        fontSize: scaleSize(12)
    },
    titleDetail: {
        fontSize: scaleSize(24),
        fontWeight: 'bold',
        color: colors.Black,
        marginLeft: scaleSize(20)
    },
    titleDetail2: {
        fontSize: scaleSize(20),
        fontWeight: 'bold',
        marginTop: scaleSize(20),
        color: colors.Black,
        marginLeft: scaleSize(20)
    },
    date: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: scaleSize(20),
        marginTop: scaleSize(20)
    },
    textDate: {
        marginLeft: scaleSize(10),
        fontSize: scaleSize(12)
    },
    wrapperPenukaran: {
        marginTop: scaleSize(0),
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '100%',
        height: 'auto',
        borderRadius: scaleSize(8),
        padding: scaleSize(20)
    },
    wrapperHighlight: {
        marginTop: scaleSize(0),
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '100%',
        borderRadius: scaleSize(8),
        padding: scaleSize(20)
    },
    footerPaket: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    priceStart: {
        color: 'red',
        fontSize: scaleSize(16),
        marginBottom: scaleSize(8),
        fontWeight: 'bold'
    },
    btnPaket: {
        width: 'auto',
        paddingHorizontal :scaleSize(16),
        alamat_paket: 'lorem', 
        paddingVertical :scaleSize(10),
        borderRadius: scaleSize(4),
        backgroundColor: '#1381B6',
        height: 'auto',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardPaket: {
        elevation: 3,
        backgroundColor: colors.White,
        padding: scaleSize(10),
        marginBottom: scaleSize(20),
        borderRadius: scaleSize(8)
    },
    peta: {
        marginHorizontal: scaleSize(20),
        overflow: 'hidden',
        height: scaleSize(160),
        borderRadius: scaleSize(10)
    },
    wrapperLocation: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: scaleSize(20)
    },
    wrapperPetunjuk: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: scaleSize(120)
    },
    childPetunjuk: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bullet: {
        borderRadius: scaleSize(999),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: scaleSize(1),
        borderColor: '#1381B6',
        width: scaleSize(45),
        height: scaleSize(45),
    },
    descFooter: {
        marginLeft: scaleSize(20),
        color: '#434F65',
        fontSize: scaleSize(14),
        width: '90%',
        lineHeight: scaleSize(20)
    },
    footerDetailEvent: {
        backgroundColor: colors.White,
        borderTopColor: '#EBEDF1',
        borderTopWidth: scaleSize(2),
        paddingHorizontal: scaleSize(20),
        paddingVertical: scaleSize(14),
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: scaleSize(80),
        justifyContent: 'space-between',
    },
    fdeLeft: {
        width: '60%'
    },
    btnfde: {
        width: scaleSize(110),
        paddingVertical: scaleSize(8),
        backgroundColor: '#1381B6',
        color: colors.White,
        borderRadius: scaleSize(4),
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3
    },
    textBtnfde: {
        color: colors.White,
    },
    textDiscountPrice: {
        color: 'red',
        fontSize: scaleSize(14),
        marginBottom: scaleSize(8)
    },
    start: {
        color: '#434F65',
        fontSize: scaleSize(14),
        marginBottom: scaleSize(4)
    },
    wrapperInfo: {
        flexDirection: 'column',
    },
    childInfo: {
        marginHorizontal: scaleSize(20),
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: scaleSize(20),
        justifyContent: 'space-between'
    },
    leftInfo: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    textInfo: {
        marginLeft: scaleSize(10)
    },
    wrapperModaStatusTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: scaleSize(20),
        marginTop: scaleSize(24),
        marginBottom: scaleSize(14)
    },
    btnStatusModal: {
        width: 24,
        height: 24,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: scaleSize(1),
        borderRadius: scaleSize(6),
    },
    X: {
        fontSize: scaleSize(14),
        top: scaleSize(-2)
    },
});

export default DetailEvent;