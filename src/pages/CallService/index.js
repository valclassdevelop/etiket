import React from 'react';
import { FlatList, Image, ScrollView, StatusBar, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Gap, Header, TextBold, TextRegular } from '../../components';
import { colors, scaleSize } from '../../utils';

const CallService = ({navigation}) => {

    const listCall = [
        {
            title: 'Whatsapp',
            img: require('../../assets/Dummy/callone.png'),
            value: '+62298378273'
        },
        {
            title: 'Email',
            img: require('../../assets/Dummy/calltwo.png'),
            value: 'etiket@address.com'
        },
        {
            title: 'Telepon',
            img: require('../../assets/Dummy/callthree.png'),
            value: '+098 7654 321'
        },
        {
            img: require('../../assets/Dummy/callfour.png'),
            value: 'Instagram'
        },
    ]

    const renderItem = ({item}) => {
        return (
            <View style={styles.cardCall}>
                <View style={styles.callLeft}>
                    <View style={styles.leftImg}>
                        <Image source={item?.img} style={styles.img} resizeMode="cover" />
                    </View>
                    <Gap width={12} />
                    <View style={styles.desc}>
                        <TextRegular text={item?.title} type="Text Regular 10" color="#434F65" />
                        <Gap height={4} />
                        <TextBold text={item?.value} color={colors.Black} type="Text Bold 14" />
                    </View>
                </View>
                <TouchableOpacity activeOpacity={0.8} style={styles.btnChat}>
                    <TextBold text="Chat" type="Text Bold 10" color={colors.White} />
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <ScrollView style={styles.container}>
            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <Header text="Hubungi Kami" onBack={() => navigation.goBack(-1)} />
            <Gap height={20} />
            <FlatList 
                data={listCall}
                renderItem={renderItem}
            />
            <Gap height={20} />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White
    },
    cardCall: {
        flexDirection: 'row',
        backgroundColor: colors.White,
        borderRadius: scaleSize(8),
        paddingHorizontal: scaleSize(12),
        paddingVertical: scaleSize(14),
        elevation: 3,
        marginHorizontal: scaleSize(20),
        marginBottom: scaleSize(20),
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    callLeft: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    leftImg: {
        width: scaleSize(50),
        height: scaleSize(50),
        overflow: 'hidden',
        borderRadius: scaleSize(8)
    },
    img: {
        width: '100%',
        height: '100%'
    },
    btnChat: {
        backgroundColor: 'rgba(19, 129, 182, 1)',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: scaleSize(6),
        paddingHorizontal: scaleSize(22),
        paddingVertical: scaleSize(6),
    },
    desc: {
        justifyContent: 'center',
    }
});

export default CallService;