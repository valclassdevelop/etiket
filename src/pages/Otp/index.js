import React, { useState } from "react";
import { Dimensions, Image, StatusBar, StyleSheet, Text, View } from "react-native";
import { Congrats, Loading, Logo } from "../../assets/Dummy";
import { Gap, Header, TextBold, TextInput, TextRegular } from "../../components";
import { BottomSheetCenter } from "../../components/BottomSheet";
import { FilledButton } from "../../components/Button";
import { colors } from "../../utils";

const Otp = () => {
    const [active, setActive] = useState(false);

    return (
        <View style={styles.container}>
        <StatusBar
            barStyle="light-content"
            backgroundColor={colors.primary}
            />
        <Header />
        <Image 
            source={Logo}
            alt="Logo aplikasi"
            style={{
                marginLeft: 20,
                width: 200,
                marginTop: -40,
                maxHeight: 200,
            }}
            resizeMode="contain"
        />
        <View style={styles.wrapperTitle}>
            <TextBold color={colors.Black} text="Verifikasi OTP" type="Title Bold 28" style={styles.titleLogin} />
            <Text style={styles.textPVerifikasi}>Verifikasi akun kamu dengan kode OTP
yang di kirim ke <Text style={styles.email}>email@address.com</Text></Text>
        </View>
        
        <View style={styles.wrapperOtp}>
            <TextInput type="verify" />
            <TextInput type="verify" />
            <TextInput type="verify" />
            <TextInput type="verify" />
            <TextInput type="verify" />
            <TextInput type="verify" />
        </View>

         
        <View style={styles.wrapperBtnLogin} onPress={() => setActive(true)}>
            <FilledButton style={styles.btnLogin} text="Verifikasi" />
        </View>

        <View style={styles.wrapperText} onClose={() => setActive(false)}>
            <Text style={styles.textNotHave}>Belum menerima OTP?</Text>
            <Text style={styles.textCreateNew}>Kirim ung</Text>
        </View>

        <BottomSheetCenter isVisibleCenter={active}>
            <View style={styles.wrapperModal}>
                <Image 
                    source={Congrats} 
                    alt="icon modal" 
                    style={{width: 160, maxHeight: 160}}
                />
                <TextRegular text="Selamat!" style={styles.CongrtsText} />
                <TextRegular text="Akun kamu siap digunakan. Kamu akan diarahkan langsung ke Home page." style={styles.textPCongrats} />

                <View style={styles.loading}>
                    <Image source={Loading} alt="Loading-img" style={{width: 40, maxHeight: 40}} />
                </View>
            </View>
        </BottomSheetCenter>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White
    },
    textPCongrats: {
        width: '90%',
        textAlign: 'center',
        marginLeft: 20,
        top: 15
    },
    loading: {
        top: 80,
    },
    CongrtsText: {
        fontSize: 24,
        fontWeight: 'bold',
        marginTop: 20,
        lineHeight: 32,
        color: '#1381B6'
    },
    wrapperModal: {
        flexDirection: 'column',
        paddingTop: 40,
        alignItems: 'center'
    },
    textPVerifikasi: {
        width: '90%',
        lineHeight: 20,
        marginLeft: 20,
    },
    email: {
        fontWeight: 'bold',
        color: '#115888'
    },
    wrapperOtp: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 20,
        marginTop: 36,
        width: Dimensions.get('window').width * 0.8,
        marginLeft: 10,
        justifyContent: 'space-between',
    },
    wrapperText: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        textAlign: 'center',
        marginTop: 24,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textCreateNew: {
        marginLeft: 4,
        fontWeight: 'bold',
        color: '#1381B6'
    },
    wrapperBtnLogin: {
        marginTop: 40,
        marginHorizontal: 20
    },
    wrapperInput: {
        marginTop: 32,
        marginHorizontal: 20
    },
    titleLogin: {
        fontWeight: 'bold',
        marginLeft: 20,
        marginBottom: 6,
    },
    pLogin: {
        width: '90%',
        marginLeft: 20,
    },
    wrapperTitle: {
        marginTop: -30,
    }
});

export default Otp;