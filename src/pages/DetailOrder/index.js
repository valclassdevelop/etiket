import { ScrollView, StatusBar, StyleSheet, TouchableOpacity, View } from 'react-native';
import React from 'react';
import { colors, NumberFormatter, scaleSize } from '../../utils';
import { Header, TextBold, TextRegular, Gap, Divider, FilledButton } from '../../components';
import { IcChevronLeft } from '../../assets';
import { Calendar } from 'react-native-calendars';

const DetailProduct = ({navigation}) => {
    return (
        <ScrollView style={styles.container}>
             <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />

            <Header text="Detail Pesanan" onBack={() => navigation.goBack(-1)}  />
            <Gap height={10} />
            <ScrollView 
                horizontal
                pagingEnabled={false}
                showsHorizontalScrollIndicator={false}
            >
                <View style={styles.choosePacket}>
                    <View style={styles.chooseTop}>
                        <TextBold text="Pilihan Paket" color={colors.Black} type="Text Bold 14" />
                        <TextRegular text="1/3" color="rgba(67, 79, 101, 1)" type="Text Regular 12" />
                    </View>
                    <Gap height={10} /> 
                    <View style={styles.contentPacket}>
                        <TextBold text="Yellow" color={colors.Black} type="Text Bold 26" />
                        <Gap height={10} />
                        <TextBold text={NumberFormatter(150000, 'IDR ')} color="red" type="Text Bold 14" />
                        <Gap height={10} />
                        <Divider height={1}r />
                        <Gap height={10} />
                        <View style={styles.childList}>
                            <IcChevronLeft width={10} height={10} fill="rgba(19, 129, 182, 1)" />
                            <Gap width={10} />
                            <TextRegular text="Konfirmasi instan" type="Text Regular 10" />
                        </View>
                        <Gap height={10} />
                        <View style={styles.childList}>
                            <IcChevronLeft width={10} height={10} fill="rgba(19, 129, 182, 1)"  />
                            <Gap width={10} />
                            <TextRegular text="Konfirmasi instan" type="Text Regular 10" />
                        </View>
                        <Gap height={10} />
                        <View style={styles.childList}>
                            <IcChevronLeft width={10} height={10} fill="rgba(19, 129, 182, 1)"  />
                            <Gap width={10} />
                            <TextRegular text="Konfirmasi instan" type="Text Regular 10" />
                        </View>
                        <Gap height={10} />
                        <View style={styles.childList}>
                            <IcChevronLeft width={10} height={10} fill="rgba(19, 129, 182, 1)"  />
                            <Gap width={10} />
                            <TextRegular text="Konfirmasi instan" type="Text Regular 10" />
                        </View>
                    </View>
                </View>
                <View style={styles.choosePacket}>
                    <View style={styles.chooseTop}>
                        <TextBold text="Pilihan Paket" color={colors.Black} type="Text Bold 14" />
                        <TextRegular text="1/3" color="rgba(67, 79, 101, 1)" type="Text Regular 12" />
                    </View>
                    <Gap height={10} /> 
                    <View style={styles.contentPacket}>
                        <TextBold text="Yellow" color={colors.Black} type="Text Bold 26" />
                        <Gap height={10} />
                        <View style={styles.childList}>
                            <IcChevronLeft width={10} height={10} fill="rgba(19, 129, 182, 1)" />
                            <Gap width={10} />
                            <TextRegular text="Konfirmasi instan" type="Text Regular 10" />
                        </View>
                        <Gap height={10} />
                        <View style={styles.childList}>
                            <IcChevronLeft width={10} height={10} fill="rgba(19, 129, 182, 1)"  />
                            <Gap width={10} />
                            <TextRegular text="Konfirmasi instan" type="Text Regular 10" />
                        </View>
                        <Gap height={10} />
                        <View style={styles.childList}>
                            <IcChevronLeft width={10} height={10} fill="rgba(19, 129, 182, 1)"  />
                            <Gap width={10} />
                            <TextRegular text="Konfirmasi instan" type="Text Regular 10" />
                        </View>
                        <Gap height={10} />
                        <View style={styles.childList}>
                            <IcChevronLeft width={10} height={10} fill="rgba(19, 129, 182, 1)"  />
                            <Gap width={10} />
                            <TextRegular text="Konfirmasi instan" type="Text Regular 10" />
                        </View>
                    </View>
                </View>
            </ScrollView>
            <Gap height={20} />
            <TextBold text="Pilih Tanggal Event" color={colors.Black} style={{marginLeft: 24}} />
            <View style={styles.calendar}>
                <Gap height={6} />
                <Calendar 
                    markingType={'custom'}
                    markedDates= {{
                        '2023-02-21': {customStyles: {
                            container: {
                                borderColor: 'rgba(26, 148, 255, 1)',
                                borderRadius: scaleSize(6),
                                borderWidth: scaleSize(1)
                            }
                        }}
                    }}
                />
            </View>
            <Gap hegiht={20} />
            <Divider height={4} />
            <Gap hegiht={20} />
            <View style={styles.totalTIcket}>
                <View style={styles.totalLeft}>
                    <TextBold text="Jumlah Tiket" color={colors.Black} type="Text Bold 14" />
                    <Gap height={20} />
                    <TextBold text="pax" color={colors.Black} type="text Bold 13" />
                    <TextBold text={NumberFormatter(150000, 'IDR ')} color="red" type="Text Bold 14" />
                </View>
                <View style={styles.totalRight}>
                    <View style={styles.wrapperBtnTotal}>
                        <TouchableOpacity activeOpacity={0.88} style={styles.min}>
                            <TextRegular text="-" color="rgba(19, 129, 182, 1)" type="Text Regular 12" />
                        </TouchableOpacity>
                        <Gap width={14} />
                        <TextBold text={1} color={colors.Black} type="Text Bold 14" />
                        <Gap width={14} />
                        <TouchableOpacity activeOpacity={0.88} style={styles.add}>
                            <TextRegular text="+" color="rgba(19, 129, 182, 1)" type="Text Regular 12" />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            <Gap height={20} />
            <Divider height={4} />
            <Gap height={20} />
            <View style={styles.result}>
                <TextRegular  text="Tanggal" color="rgba(67, 79, 101, 1)" type="Text Regular 14" />
                <TextBold text="31 January 2023" color={colors.Black} type="Text Bold 14" />
            </View>
            <Gap height={20} />
            <View style={styles.result}>
                <TextRegular  text="Total Harga" color="rgba(67, 79, 101, 1)" type="Text Regular 14" />
                <TextBold text="31 January 2023" color={colors.Black} type="Text Bold 14" />
            </View>
            <Gap height={100} />
            <View style={styles.wrapperBtnFooter}>
                <FilledButton text="Pesan" backgroundColor='rgba(19, 129, 182, 1)' onPress={() => navigation.navigate('Checkout')} textColor={colors.White} />
            </View>
            <Gap height={30} />
        </ScrollView>        
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White
    },
    choosePacket: {
        backgroundColor: "rgba(19, 129, 182, 0.1)",
        justifyContent: 'center',
        paddingVertical: scaleSize(20),
        height: 'auto',
        width: scaleSize(370)
    },
    chooseTop: {
        paddingHorizontal: scaleSize(24),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    contentPacket: {
        backgroundColor: colors.White,
        marginHorizontal: scaleSize(24),
        borderColor: 'rgba(19, 129, 182, 1)',
        borderWidth: scaleSize(1),
        borderRadius: scaleSize(8),
        padding: scaleSize(20)
    },
    childList: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    calendar: {
        paddingHorizontal: scaleSize(10),
    },
    totalTIcket: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: scaleSize(24),
        paddingVertical: scaleSize(20),
        justifyContent: 'space-between'
    },
    wrapperBtnTotal: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    add: {
        borderWidth: scaleSize(1),
        borderRadius: scaleSize(6),
        alignItems: 'center',
        justifyContent: 'center',
        width: scaleSize(26),
        height: scaleSize(26),
        borderColor: 'rgba(19, 129, 182, 1)'
    },
    min: {
        borderWidth: scaleSize(1),
        borderRadius: scaleSize(6),
        alignItems: 'center',
        justifyContent: 'center',
        width: scaleSize(26),
        height: scaleSize(26),
        borderColor: 'rgba(182, 198, 227, 1)'
    },
    result: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: scaleSize(24)
    },
    wrapperBtnFooter: {
        marginHorizontal: scaleSize(24),
    }
});

export default DetailProduct