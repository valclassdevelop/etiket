
import React from "react";
import { Dimensions, Image, StatusBar, StyleSheet, Text, View } from "react-native";
import { Logo } from "../../assets/Dummy";
import { Header, TextBold, TextInput, TextRegular } from "../../components";
import { FilledButton } from "../../components/Button";
import { colors } from "../../utils";

const ForgotPassword = () => {
    return (
        <View style={styles.container}>
        <StatusBar
            barStyle="light-content"
            backgroundColor={colors.primary}
            />
        <Header />
        <Image 
            source={Logo}
            alt="Logo aplikasi"
            style={{
                marginLeft: 20,
                width: 200,
                marginTop: -40,
                maxHeight: 200,
            }}
            resizeMode="contain"
        />
        <View style={styles.wrapperTitle}>
            <TextBold color={colors.Black} text="Lupa Password" type="Title Bold 28" style={styles.titleLogin} />
            <TextRegular style={styles.pLogin} type="Text Reguler 16" color="#434F65" text="Masukkan email kamu untuk menerima link reset password" />
        </View>
        <View style={styles.wrapperInput}>
            <TextInput label="Email" placeholder="Email" />
        </View>
        
        <View style={styles.wrapperBtnLogin}>
            <FilledButton style={styles.btnLogin} text="Dapatkan Link" />
        </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White
    },
    wrapperText: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        textAlign: 'center',
        marginTop: 24,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textPCongrats: {
        width: '90%',
        textAlign: 'center',
        marginLeft: 20,
        top: 15
    },
    loading: {
        top: 80,
    },
    CongrtsText: {
        fontSize: 24,
        fontWeight: 'bold',
        marginTop: 20,
        lineHeight: 32,
        color: '#1381B6'
    },
    wrapperModal: {
        flexDirection: 'column',
        paddingTop: 40,
        alignItems: 'center'
    },
    textCreateNew: {
        marginLeft: 4,
        fontWeight: 'bold',
        color: '#1381B6'
    },
    wrapperBtnLogin: {
        marginTop: 40,
        marginHorizontal: 20
    },
    wrapperInput: {
        marginTop: 32,
        marginHorizontal: 20
    },
    titleLogin: {
        fontWeight: 'bold',
        marginLeft: 20,
        marginBottom: 6,
    },
    pLogin: {
        width: '90%',
        marginLeft: 20,
    },
    wrapperTitle: {
        marginTop: -30,
    }
});

export default ForgotPassword;