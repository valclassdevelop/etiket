import React, {useState, useRef, useEffect} from 'react';
import {
  FlatList,
  StyleSheet,
  View,
  Animated,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Sliders from '../../assets/JSON/onBoarding.js';
import {OnBoardingContent, TextBold, TextRegular} from '../../components';
import {colors} from '../../utils';

const OnBoarding = ({navigation}) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const scrollX = useRef(new Animated.Value(0)).current;
  const slidersRef = useRef(null);
  const [activeEnd, setActiveEnd] = useState(false);

  const viewableItemsChanged = useRef(({viewableItems}) => {
    setCurrentIndex(viewableItems[0].index);
  }).current;

  const viewConfig = useRef({viewAreaCoveragePercentThreshold: 50}).current;

  useEffect(() => {
    if (currentIndex === 2) {
      setActiveEnd(true);
    }
  }, [currentIndex]);

  const scrollTo = () => {
    if (currentIndex < Sliders.length - 1) {
      slidersRef.current.scrollToIndex({index: currentIndex + 1});
    } else if (currentIndex === 1) {
      setActiveEnd(true);
    } else {
      navigation.replace('Homepage');
    }
  };

  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor={colors.White} />
      <View style={styles.page}>
        <View activeOpacity={0.8} >
          <TouchableOpacity activeOpacity={0.9} style={styles.skipText} onPress={() => navigation.navigate('Homepage')}>
            <TextBold text="Skip" type="Text Bold 14" color={"#115888"} />
          </TouchableOpacity>
        </View>

        <FlatList
          ref={slidersRef}
          data={Sliders}
          renderItem={({item}) => (
            <OnBoardingContent
              data={item}
              dots={Sliders}
              scrollX={scrollX}
              status={activeEnd}
              scrollTo={scrollTo}
            />
          )}
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          scrollEnabled={false}
          bounces={false}
          keyExtractor={item => item?.id}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {x: scrollX}}}],
            {useNativeDriver: false},
          )}
          onViewableItemsChanged={viewableItemsChanged}
          viewabilityConfig={viewConfig}
        />
      </View>
    </>
  );
};

export default OnBoarding;

const styles = StyleSheet.create({
  page: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'flex-end',
    backgroundColor: colors.White,
  },
  skip: {
    width: '100%',
    height: 'auto',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.White,
    justifyContent: 'flex-end',
  },
  skipText: {
    marginRight: 20,
    fontFamily: 'Inter-Regular',
    marginTop: 40,
    fontWeight: '600',
    color: '#115888',
  },
});
