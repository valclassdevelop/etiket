import React, { useState } from 'react';
import { StyleSheet, ScrollView, FlatList, View, StatusBar, TouchableOpacity } from 'react-native';
import { IcBell } from '../../assets';
import DataNotification from '../../assets/JSON/DataNotification';
import { Divider, BottomSheet, Gap, Header, TextBold, TextRegular } from '../../components';
import { colors, scaleSize } from '../../utils';

const Notification = () => {

    const [activeModal, setActiveModal] = useState(false);

    const renderItem = ({item}) => {
        return (
            <TouchableOpacity activeOpacity={0.8} style={styles.cardNotification} onPress={() => setActiveModal(true)}>
                <View style={styles.cardTop}>
                    <View style={styles.topLeft}>
                        <View style={styles.iconLeft}>
                            <IcBell width={30} height={30} fill="#1381B6" />
                        </View>
                        <Gap width={12} />
                        <View style={styles.descLeft}>
                            <TextBold text={item?.title} color={colors.Black} type="Text Bold 16" />
                            <Gap height={2} />
                            <TextRegular text={item?.date} type="Text Regular 12" />
                        </View>
                    </View>
                    {
                        item?.status ? (
                            <TouchableOpacity activeOpacity={0.8} style={styles.btnNew}>
                                <TextRegular text={item?.status} type="Text Regular 10" color={colors.White} />
                            </TouchableOpacity>
                        ):
                            <></>
                    }
                </View>
                <Gap height={10} />
                <TextRegular 
                    text={item?.desc}
                    color="#434F65"
                    type="Text Regular 14"
                />
            </TouchableOpacity>
        )
    }

    return (
        <ScrollView style={styles.container}>
            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <Header text="Notification" onPress={() => navigation.goBack(-1)} />
            <Gap height={20} />
            <FlatList 
                data={DataNotification}
                renderItem={renderItem}
            />

            <BottomSheet 
                height={600}
                isVisible={activeModal}
                onClose={() => setActiveModal(false)}
            >
                 <View style={styles.wrapperModaStatusTop}>
                    <View>
                        <TextBold text="Booking Berhasil!" color="#000000" style={styles.titleModalStatus} />
                        <Gap height={4} />
                        <TextRegular text="30 Jan, 2023 | 07.17 WIB" type="Text Regular 12" />
                    </View>
                    <TouchableOpacity 
                        activeOpacity={0.9} 
                        onPress={() => setActiveModal(false)} 
                        style={styles.btnStatusModal}
                    >
                        <TextRegular color="rgba(67, 79, 101, 1)" text="x" type="Text Regular 12" />
                    </TouchableOpacity>
                </View>
                <Divider height={1} />
                <Gap height={30} />
                <View style={styles.contentModal}>
                    <TextRegular 
                        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." 
                        type="Text Regular 14"
                    />
                </View>
            </BottomSheet>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White
    },
    cardNotification: {
        marginHorizontal: scaleSize(20),
        height: 'auto',
        marginBottom: scaleSize(40),
    },
    cardTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    topLeft: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    iconLeft: {
        borderRadius: scaleSize(999),
        alignItems: 'center',
        backgroundColor: 'rgba(19, 129, 182, 0.16)',
        width: scaleSize(50),
        height: scaleSize(50),
        justifyContent: 'center',
    },
    btnNew: {
        borderRadius: scaleSize(100),
        backgroundColor: 'rgba(19, 129, 182, 1)',
        paddingHorizontal: scaleSize(15),
        paddingVertical: scaleSize(6)
    },
    wrapperModaStatusTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: scaleSize(20),
        marginTop: scaleSize(24),
        marginBottom: scaleSize(14)
    },
    btnStatusModal: {
        width: 24,
        height: 24,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: scaleSize(1),
        borderRadius: scaleSize(6),
    },
    X: {
        fontSize: scaleSize(14),
        top: scaleSize(-2)
    },
    contentModal: {
        marginHorizontal: scaleSize(20)
    }
});

export default Notification;