import React, { useState } from "react";
import { Dimensions, Text, FlatList, Image, ScrollView, StatusBar, StyleSheet, TouchableOpacity, View } from "react-native";
import { FlatListSlider } from 'react-native-flatlist-slider';
import { IcBookmark2, IcFilter, IcFilterSearch, IcLocation, IcNotification, IcSearch } from "../../assets";
import { Circles, Discount, Play, Spiral, User } from "../../assets/Dummy";
import { Preview, TextBold, TextRegular, FilledButton, Gap } from "../../components";
import { TextInput } from "react-native";
import { colors, scaleSize, NumberFormatter } from "../../utils";
import Menu from "./Components/Menu.js";
import TitleSection from "./Components/TitleSection";
import Promo from "../../assets/JSON/Promo";
import Event from "../../assets/JSON/Event";


const Homepage = ({navigation}) => {

    const listMenu = [
        {name: 'All'},
        {name: 'Fruit'},
        {name: 'Vegetable'},
        {name: 'Meat'},
        {name: 'Drink'},
        {name: 'Biscuit'},
    ];
    
    const [active, setActive] = useState(listMenu[0].name);

    const images = [
        {
         image: require('../../assets/Dummy/card1.png'),
        },
        {
         image: require('../../assets/Dummy/card1.png'),
        },
    ];

    const onPress = value => {
        setActive(value);
    };

    const renderItem = ({item}) => {
        return (
            <TouchableOpacity activeOpacity={0.8} style={styles.cardContainer}>
                <View style={styles.cardTop}>
                    <Image source={item?.image} style={styles.imgCard} alt="image-card" resizeMode='cover' />
                </View>
                <View style={styles.cardBottom}>
                    <View style={styles.location}>
                        <IcLocation width={14} height={14} />
                        <TextRegular text={item?.location} style={styles.textLocation} />
                    </View>
                    <TextBold text={item?.title} style={styles.titleCard} />
                    <TextRegular text={item?.time} color="rgba(17, 88, 136, 1)" style={styles.textDate} />
                    
                    <View style={styles.footerCard}>
                        <TextRegular text="Harga mulai dari" color="" style={styles.priceStart} />
                        <TextRegular text={"IDR " + NumberFormatter(item?.discount, 'Rp')} color="" style={styles.textDiscountPrice} />
                        <TextBold text={NumberFormatter(item?.price, 'Rp')} style={styles.titlePrice} />
                    </View>
                
                </View>
            </TouchableOpacity>
        );
      };

    const renderItem3 = ({item}) => {
        return (
            <TouchableOpacity activeOpacity={0.8} style={styles.cardContainer3}>
                <View style={styles.cardTop3}>
                    <Image source={item?.image} style={styles.imgCard} alt="image-card" resizeMode='cover' />
                </View>
                <View style={styles.cardBottom}>
                    <View style={styles.location}>
                        <IcLocation width={14} height={14} />
                        <TextRegular text={item?.location} style={styles.textLocation} />
                    </View>
                    <TextBold text={item?.title} style={styles.titleCard} />
                    <TextRegular text={item?.time} color="rgba(17, 88, 136, 1)" style={styles.textDate} />
                    
                    <View style={styles.footerCard}>
                        <TextRegular text="Harga mulai dari" color="" style={styles.priceStart3} />
                        <TextBold text={NumberFormatter(item?.price, 'Rp')} style={styles.titlePrice3} />
                    </View>
                
                </View>
            </TouchableOpacity>
        );
      };

    const renderItem4 = ({item}) => {
        return (
            <TouchableOpacity activeOpacity={0.8} style={styles.cardContainer4}>
                <View style={styles.cardTop4}>
                    <Image source={item?.image} style={styles.imgCard} alt="image-card" resizeMode='cover' />
                </View>
                <View style={styles.cardBottom}>
                    <View style={styles.location}>
                        <IcLocation width={14} height={14} />
                        <TextRegular text={item?.location} style={styles.textLocation} />
                    </View>
                    <TextBold text={item?.title} style={styles.titleCard} />
                    <TextRegular text={item?.time} color="rgba(17, 88, 136, 1)" style={styles.textDate} />
                    
                    <View style={styles.footerCard}>
                        <TextRegular text="Harga mulai dari" color="" style={styles.priceStart3} />
                        <TextBold text={NumberFormatter(item?.price, 'Rp')} style={styles.titlePrice3} />
                    </View>
                
                </View>
            </TouchableOpacity>
        );
      };

    const renderItem2 = ({item}) => {
        return (
            <TouchableOpacity activeOpacity={0.8} style={styles.cardContainer2}>
                <View style={styles.cardTop2}>
                    <Image source={item?.image} style={styles.imgCard} alt="image-card" resizeMode='cover' />
                </View>
                <View style={styles.cardBottom}>
                    <View style={styles.location}>
                        <IcLocation width={14} height={14} />
                        <TextRegular text={item?.location} style={styles.textLocation} />
                    </View>
                    <TextBold text={item?.title} style={styles.titleCard} />
                    <TextRegular text={item?.time} color="rgba(17, 88, 136, 1)" style={styles.textDate} />
                
                </View>
            </TouchableOpacity>
        );
      };

    return (
            <ScrollView style={styles.container}>

            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />

            <View style={styles.headerHome}>
                <View style={styles.wrapperHeader}>
                    <View style={styles.headerLeft}>
                        <Image source={User} style={{width: 50, height: 50, marginRight: 16}} />
                        <View style={styles.headerLeft1}>
                            <TextRegular color={colors.White} text="Selamat datang" type="Text Regular 10" />
                            <TextBold text="SHohei" color={colors.White} type="Title Bold 20" />
                        </View>
                    </View>
                    <View style={styles.headerRight}>
                        <TouchableOpacity style={styles.bell} activeOpacity={0.8} onPress={() => navigation.navigate('Notification')}>
                            <IcNotification width={23} height={23} />
                        </TouchableOpacity>
                    </View>
                </View> 

                <Image source={Spiral} alt="partikel-spiral" style={styles.spiral} />
                <Image source={Spiral} alt="partikel-spiral" style={styles.spiral2} />
                <Image source={Circles} alt="partikel-spiral" style={styles.circles} />

                <View style={styles.wrapperSearch}>
                    <View style={styles.searchLeft}>
                        <IcSearch width={20} height={20} style={{marginRight: scaleSize(10)}} stroke={colors.White} />
                        <TextInput placeholder="Cari event" style={{width: '100%'}} placeholderTextColor="#ffff"  />
                    </View>
                    <View style={styles.searchRight}>
                        <IcFilterSearch width={20} height={20} stroke={colors.White} />
                    </View>
                </View>
                <View style={styles.sliders}>
                    <FlatListSlider
                        data={images}
                        width={305}
                        timer={5000}
                        showsVerticalScrollIndicator={false}
                        shownHorizontalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        component={<Preview />}
                        onPress={item => alert(JSON.stringify(item))}
                        contentContainerStyle={{paddingHorizontal: 16}}
                    />
                </View>

            </View>
            
                <Menu />

                <TitleSection title="PROMO E-TIKET" icon={<Image source={Discount} style={{width: 24, maxHeight: 24}} />} textRight="Lihat Semua" />
                <TextRegular text="Happy New Year, rayakan tahun barumu dengan segudang promo dari etiket." style={{width: '90%', marginTop: scaleSize(12), fontSize: scaleSize(14), marginLeft: scaleSize(20)}} />
                
                <View style={styles.wrapperPromo}>
                    <FlatList
                        horizontal
                        pagingEnabled={false}
                        showsHorizontalScrollIndicator={false}
                        data={Promo}
                        renderItem={renderItem}
                    />
                </View>

                <View style={styles.event}>
                    <View style={styles.titleEvent}>
                        <Image source={Play} alt="icon play" style={{width: 20, height: 20}} />
                        <TextBold text="LIVE EVENT" style={styles.titleEvents} />
                    </View>
                    <View style={styles.wrapperEvent}>
                        <FlatList
                            horizontal
                            pagingEnabled={false}
                            showsHorizontalScrollIndicator={false}
                            data={Event}
                            renderItem={renderItem2}
                        />
                    </View>
                    <Gap height={20} />
                    <FilledButton textColor="rgba(19, 129, 182, 1)" text="Lihat Semua" backgroundColor={colors.White} />
                    <Image source={Spiral} alt="partikel-spiral" style={styles.spiralEvent} />
                    <Image source={Spiral} alt="partikel-spiral" style={styles.spiralEvent2} />
                    <Image source={Circles} alt="partikel-spiral" style={styles.circlesEvent} />
                    <Image source={Circles} alt="partikel-spiral" style={styles.circlesEvent2} />
                </View>

                <Gap height={12} />

                <TitleSection title="NEXT EVENT" icon={<IcBookmark2 width={20} height={20} />} textRight="Lihat Semua" />
                <ScrollView
                    contentContainerStyle={styles.contentContainer}
                    pagingEnabled={false}
                    horizontal
                    shownHorizontalScrollIndicator={false}>
                    <Gap height={4} />
                    <View style={styles.menuTop}>
                        {listMenu.map((menu, index) => (
                        <View key={index}>
                            <TouchableOpacity activeOpacity={0.8}>
                            <Text
                                onPress={() => onPress(menu.name)}
                                style={styles.textBar(active, menu.name)}
                                name={menu.name}>
                                {menu.name}
                            </Text>
                            </TouchableOpacity>
                        </View>
                        ))}
                    </View>
                </ScrollView>
                <View style={styles.wrapperPromo}>
                    <FlatList
                        horizontal
                        pagingEnabled={false}
                        showsHorizontalScrollIndicator={false}
                        data={Promo}
                        renderItem={renderItem3}
                    />
                </View>
                
                <TitleSection title="NEW EVENT" icon={<IcBookmark2 width={20} height={20} />} textRight="Lihat Semua" />

                <View style={styles.wrapperNewEvent}>
                    <FlatList
                        vertical
                        numColumns={2}
                        pagingEnabled={false}
                        showsHorizontalScrollIndicator={false}
                        data={Promo}
                        columnWrapperStyle={{  flex: 1,justifyContent: "space-between", paddingBottom: scaleSize(20)}}
                        ItemSeparatorComponent={() => <View style={{height: 20}} />}
                        renderItem={renderItem4}
                    />
                </View>

            </ScrollView>

        )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.White,
        flex: 1,
        paddingBottom: scaleSize(140)
    },
    wrapperPromo: {
        height: scaleSize(310),
        width: 'auto',
        paddingLeft: scaleSize(20),
        marginTop: scaleSize(16)
    },
    wrapperNewEvent: {
        height: 'auto',
        width: 'auto',
        paddingBottom: scaleSize(90),
        marginHorizontal: scaleSize(20),
        marginTop: scaleSize(16)
    },
    wrapperEvent: {
        position: 'relative',
        height: 'auto',
        width: scaleSize(326),
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: scaleSize(16)
    },
    titleEvent: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    textBar: (active, menu) => ({
        fontSize: scaleSize(14),
        paddingHorizontal: scaleSize(16),
        paddingVertical: scaleSize(6),
        alignItems: 'center',
        alignSelf: 'baseline',
        textAlign: 'center',
        fontFamily: 'Poppins-Black',
        color: active === menu ? 'white' : '#1381B6',
        backgroundColor: active === menu ? '#1381B6' : "transparent",
        borderColor: '#1381B6',
        borderWidth: scaleSize(1),
        borderRadius: scaleSize(36),
        marginRight: scaleSize(10),
    }),
    menuTop: {
        alignSelf: 'baseline',
        flexDirection: 'row',
        marginLeft: scaleSize(20),
        paddingBottom: scaleSize(4),
        marginTop: scaleSize(14),
        alignItems: 'center',
        fontFamily: 'Poppins-Regular',
    },
    event: {
        width: Dimensions.get('window').width,
        height: scaleSize(385),
        backgroundColor: 'rgba(19, 129, 182, 1)',
        padding: scaleSize(24)
    },
    titleEvents: {
        color: colors.White,
        fontWeight: 'bold',
        fontSize: scaleSize(18),
        top: scaleSize(2),
        marginLeft: scaleSize(8)
    },
    wrapperHeader: {
        paddingVertical: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        zIndex: 999999999999,
        position: 'relative'
    },
    headerHome: {
        backgroundColor: '#1381B6',
        width: Dimensions.get('window').width,
        flexDirection: 'column',
        zIndex: -1,
        justifyContent: 'space-between',
        alignItems: 'center',
        height: scaleSize(480),
        paddingHorizontal: scaleSize(24),
        paddingVertical: scaleSize(20),
    },
    spiralEvent: {
        position: 'absolute',
        width: scaleSize(220),
        maxHeight: scaleSize(220),
        top: scaleSize(-55),    
        right: scaleSize(-65),
        zIndex: scaleSize(-1)
     },
    spiralEvent2: {
        position: 'absolute',
        width: scaleSize(180),
        maxHeight: scaleSize(180),
        bottom: scaleSize(-65),    
        left: scaleSize(-65),
        zIndex: scaleSize(-1)
     },
    circlesEvent: {
        position: 'absolute',
        width: scaleSize(180),
        maxHeight: scaleSize(180),
        top: scaleSize(-55),    
        left: scaleSize(-75),
        zIndex: scaleSize(-1)
     },
    circlesEvent2: {
        position: 'absolute',
        width: scaleSize(180),
        maxHeight: scaleSize(180),
        bottom: scaleSize(-75),    
        right: scaleSize(-75),
        zIndex: scaleSize(-1)
     },
    spiral: {
        position: 'absolute',
        width: scaleSize(220),
        maxHeight: scaleSize(220),
        top: scaleSize(50),    
        right: scaleSize(50)
     },
    spiral2: {
        position: 'absolute',
        width: scaleSize(180),
        maxHeight: scaleSize(180),
        bottom: scaleSize(-80),    
        left: scaleSize(-50)
     },
    circles: {
        position: 'absolute',
        width: scaleSize(140),
        maxHeight: scaleSize(140),
        bottom: scaleSize(-50),    
        right: scaleSize(0)
     },
    headerLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '50%'
    },
    name: {
        fontWeight: 'bold',
        fontSize: scaleSize(22),
        marginTop: scaleSize(6)
    },
    headerRight: {
        width: '50%',
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    bell: {
        width: scaleSize(44),
        height: scaleSize(44),
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: scaleSize(99)
    },
    wrapperSearch: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.25)',
        borderRadius: 99,
        height: 'auto',
        marginTop: scaleSize(2),
        width: Dimensions.get('window').width * 0.88
    },
    searchLeft: {
        flexDirection: 'row',
        width: '80%',
        alignItems: 'center'
    },
    sliders: {
        marginTop: -235,
    },
    cardContainer2: {
        width: 148,
        marginRight: scaleSize(16),
        height: scaleSize(220),
        padding: scaleSize(8),
        borderRadius: scaleSize(12),
        position: 'relative',
        overflow: 'hidden',
        elevation: 3,
        backgroundColor: colors.White
    },
    cardTop2: {
        borderRadius: scaleSize(12),
        overflow: 'hidden',
        height: '60%',
        width: '100%',
    },
    cardContainer3: {
        width: 168,
        marginRight: scaleSize(16),
        height: scaleSize(280),
        padding: scaleSize(8),
        borderRadius: scaleSize(12),
        position: 'relative',
        overflow: 'hidden',
        elevation: 3,
        backgroundColor: colors.White
    },
    cardTop3: {
        borderRadius: scaleSize(12),
        overflow: 'hidden',
        height: '50%',
        width: '100%',
    },
    cardContainer4: {
        width: '48%',
        // marginRight: scaleSize(16),
        height: scaleSize(280),
        padding: scaleSize(8),
        borderRadius: scaleSize(12),
        position: 'relative',
        overflow: 'hidden',
        elevation: 3,
        backgroundColor: colors.White
    },
    cardTop4: {
        borderRadius: scaleSize(12),
        overflow: 'hidden',
        height: '50%',
        width: '100%',
    },
    cardContainer: {
        width: scaleSize(180),
        height: scaleSize(280),
        marginRight: scaleSize(10),
        padding: scaleSize(8),
        borderRadius: scaleSize(12),
        position: 'relative',
        overflow: 'hidden',
        elevation: 3,
        backgroundColor: colors.White
    },
    cardTop: {
        borderRadius: scaleSize(12),
        overflow: 'hidden',
        height: '40%',
        width: '100%',
    },
    imgCard: {
        height: '100%',
        width: '100%'
    },
    titleCard: {
        color: colors.Black,
        fontSize: scaleSize(12),
        fontWeight: 'bold',
        marginTop: scaleSize(4),
        marginVertical: scaleSize(12)
    },
    location: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: scaleSize(8)
    },
    textLocation: {
        fontSize: scaleSize(10),
        marginLeft: scaleSize(4),
        color: 'rgba(67, 79, 101, 1)'
    },
    textDate: {
        fontSize: scaleSize(10),
        marginTop: scaleSize(-12),
        color: 'rgba(67, 79, 101, 1)',
        marginBottom: scaleSize(20)
    },
    priceStart: {
        color: 'rgba(158, 157, 159, 1)',
        fontSize: scaleSize(10),
        marginBottom: scaleSize(8)
    },
    priceStart3: {
        color: 'rgba(158, 157, 159, 1)',
        fontSize: scaleSize(10),
        marginTop: scaleSize(-6),
        marginBottom: scaleSize(8)
    },
    textDiscountPrice: {
        textDecorationLine: 'line-through',
        fontSize: scaleSize(10),
        marginTop: scaleSize(-8),
        color: 'rgba(67, 79, 101, 1)'
    },
    titleStart: {
        color: 'red',
        fontSize: scaleSize(16),
        fontWeight: 'bold'
    },
    footerCard: {
        marginTop: scaleSize(-5)
    },
    titlePrice: {
        color: 'red',
        fontWeight: 'bold'
    },
    titlePrice3: {
        top: scaleSize(-8),
        color: 'red',
        fontWeight: 'bold'
    }
});

export default Homepage;