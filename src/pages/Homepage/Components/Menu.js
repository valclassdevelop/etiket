import {Animated, Dimensions, Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors, scaleSize} from '../../../utils';
import {Gap, TextMedium} from '../../../components';
import DATA from '../../../assets/JSON/Menu';
import {useNavigation} from '@react-navigation/native';

const Menu = ({height}) => {
  const navigate = useNavigation();

  const onNavigate = action => {
    navigate.navigate(action);
  };
  return (
    <Animated.View style={[styles.content, {height}]}>
      <View style={styles.row}>
        {DATA.map((item, i) => (
          <TouchableOpacity
            key={i}
            activeOpacity={0.8}
            style={styles.menu}
            onPress={() => onNavigate(item?.action)}>
            <Image style={{width: 40, maxHeight: 40}} source={item?.image} alt="icon-menu" />
            <Gap height={10} />
            <TextMedium
              type="Text Medium 12"
              text={item.title}
              color={colors.Black}
              style={styles.center}
            />
          </TouchableOpacity>
        ))}
      </View>
    </Animated.View>
  );
};

export default Menu;

const styles = StyleSheet.create({
  content: {
    width: Dimensions.get('window').width,
    backgroundColor: colors.White,
    maxHeight: 230,
    zIndex: 1,
    marginTop: scaleSize(-140),
    borderTopLeftRadius: scaleSize(16),
    borderTopRightRadius: scaleSize(16),
    paddingHorizontal: scaleSize(16),
    paddingTop: scaleSize(14),
    paddingBottom: scaleSize(18),
  },
  row: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  menu: {
    width: '18%',
    height: 'auto',
    alignItems: 'center',
    marginBottom: scaleSize(32),
  },
  center: {
    textAlign: 'center',
  },
});

