import React from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import { TextBold, TextRegular } from '../../../components';
import { scaleSize } from '../../../utils';

const TitleSection = ({icon, title, textRight, onPress, style, colors = 'rgba(19, 129, 182, 1)'}) => {
    return (
        <View style={styles.wrapperTitle}>
            <View style={styles.titleLeft}>
                {icon}
                <TextBold text={title} type="Text Bold 16" style={styles.title} />
            </View>
            <View style={styles.titleRight}>
                <TextRegular text={textRight} color={colors} style={styles.textRight} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapperTitle: {
        height: scaleSize(40),
        width: Dimensions.get('window').width,
        paddingHorizontal: scaleSize(20),
        flexDirection: 'row',
        alignItems: 'center',
        justifyConten: 'space-between'
    },
    title: {
        color: '#000',
        fontSize: scaleSize(16),
        fontWeight: 'bold',
        marginLeft: scaleSize(12)
    },
    titleLeft: {
        width: '70%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    titleRight: {
        width: '30%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    textRight: {
        fontSize: scaleSize(14),
        fontWeight: '600'
    }
});

export default TitleSection;