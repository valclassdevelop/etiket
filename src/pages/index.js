import OnBoarding from "./OnBoarding";
import Login from "./Login";
import Register from "./Register";
import Homepage from "./Homepage";
import Otp from "./Otp";
import ForgotPassword from "./ForgotPassword";
import CreatePassword from "./CreatePassword";
import Event from "./Event";
import Transaksi from "./Transaksi";
import Profile from "./Profile";
import DetailEvent from "./DetailEvent";
import DetailEtiket from "./DetailEtiket";
import Checkout from "./Checkout";
import OtherPayment from "./OtherPayment";
import ContinuePayment1 from "./ContinuePayment1";
import ContinuePayment2 from "./ContinuePayment2";
import PaymentSuccess from "./PaymentSuccess";
import ListOrder from "./ListOrder";
import DetailTransaksi from "./DetailTransaksi";
import EditProfile from "./EditProfile";
import CallService from "./CallService";
import FaqService from "./FaqService";
import MyOrder from "./MyOrder";
import Etiket from "./Etiket";
import DetailOrder from "./DetailOrder";
import Notification from "./Notification";
import ShowOrder from "./ShowOrder";

export {
    ShowOrder,
    DetailOrder,
    Notification,
    MyOrder,
    FaqService,
    CallService,
    EditProfile,
    Profile,
    DetailTransaksi,
    PaymentSuccess,
    ListOrder,
    DetailEtiket,
    OtherPayment,
    ContinuePayment1,
    ContinuePayment2,
    OnBoarding,
    Login,
    Checkout,
    Etiket,
    Register,
    DetailEvent,
    Homepage,
    Otp,
    ForgotPassword,
    CreatePassword,
    Event,
    Transaksi,
}