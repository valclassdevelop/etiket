import React, { useEffect, useState } from 'react';
import { Image, ScrollView, LayoutAnimation, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { IcChevronDown, IcChevronUp, IcLocation } from '../../assets';
import { Banner, BCA, Check } from '../../assets/Dummy';
import DataCollapse1 from '../../assets/JSON/DataCollapse1';
import { Divider, FilledButton, Gap, Header, TextBold, TextInput, TextRegular } from '../../components';
import Radio from '../../components/Radio';
import { colors, NumberFormatter, scaleSize } from '../../utils';


const ContinuePayment2 = ({navigation}) => {
    const [activeStep1, setActiveStep1] = useState(false);
    const [activeStep2, setActiveStep2] = useState(false);
    const [activeStep3, setActiveStep3] = useState(false);
    const [success, setSuccess] = useState(true);
    const [isShow, setIsShow] = useState(null);

    const handleAccordion = item => {
      if (item?.id === isShow) {
        LayoutAnimation.easeInEaseOut();
        setIsShow(null);
      } else {
        LayoutAnimation.easeInEaseOut();
        setIsShow(item?.id);
      }
    };
  

    // useEffect(() => {
    //     navigation.navigate('PaymentSuccess')
    // }, [success])

    return (
        <ScrollView style={styles.container}>
            <Header text="Lanjutkan Pembayaran" onBack={() => navigation.goBack(-1)} />
            <Gap height={20} />
           <View style={styles.wrapperStep}>
                <View style={styles.step}>
                    <Image source={Check} alt="success" style={{width: scaleSize(24), height: scaleSize(24)}} />
                    <Gap width={4} />
                    <TextRegular text="Pilih metode" type="Text Regular 12" />
                    <View style={{width: 20, height: scaleSize(1), backgroundColor: 'rgba(182, 198, 227, 1)', top: scaleSize(2), left: scaleSize(10)}} />
                </View>
                <View>
                    <Divider height={2} />
                </View>
                <View style={[styles.step, {left: scaleSize(10)}]}>
                    <View style={styles.numberStep2(activeStep2)}>
                        <TextRegular text="2" color={colors.White} type="Text Regular 12" />
                    </View>
                    <TextRegular text="Bayar" type="Text Regular 12" />
                    <View style={{width: 20, height: scaleSize(1), backgroundColor: 'rgba(182, 198, 227, 1)', top: scaleSize(2), left: scaleSize(10)}} />
                </View>
                <View>
                    <Divider height={2} />
                </View>
                <View style={styles.step}>
                    <View style={styles.numberStep(activeStep3)}>
                        <TextRegular text="3" color={colors.Black} type="Text Regular 12" />
                    </View>
                    <TextRegular text="Selesai" type="Text Regular 12" />
                </View>
           </View>
           <Gap height={24} />
           <View style={styles.cardCheck}>
                <View style={styles.cardCheckTop}>
                    <TextBold text="Selesaikan dalam" color={colors.Black} type="Text Bold 14" />
                    <View style={styles.wrapperTimes}>
                        <View style={styles.childTimes}>
                            <TextRegular text="00" type="Text Regular 10" color={colors.White} />
                        </View>                        
                            <TextRegular text=":" style={{marginHorizontal: scaleSize(4)}} />
                        <View style={styles.childTimes}>
                            <TextRegular text="59" type="Text Regular 10" color={colors.White} />
                        </View>                        
                            <TextRegular text=":" style={{marginHorizontal: scaleSize(4)}} />
                        <View style={styles.childTimes}>
                            <TextRegular text="49" type="Text Regular 10" color={colors.White} />
                        </View>                        
                    </View>
                </View>
                <Gap height={16} />
                <View style={styles.cardCheckBottom}> 
                    <View style={styles.cardTop2}>
                        <View style={styles.imgCheckout}>
                            <Image source={Banner} alt="img-product" style={{width: '100%', maxHeight: '100%'}} resizeMode="cover" />
                        </View>
                        <Gap width={10} />
                        <View style={styles.cardTop3}>
                            <TextBold color={colors.Black} text="National Music Festival" type="Title Bold 14" />
                            <Gap height={2} />
                            <TextRegular text="Mon, Dec 23 - 18.00 - 23.00 PM" color="#115888" type="Text Regular 10" />
                            <Gap height={8} />
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <IcLocation width={8} height={8} />
                                <Gap width={8} />
                                <TextRegular text="Jakarta, Indonesia" color="#434F65" type="Text Regular 10" />
                            </View>
                        </View>
                    </View>
                    <Gap height={16} />
                </View>
           </View>
           <Gap height={16} />
           <View style={styles.cardNext}>
                <TextBold text="Lanjutkan Transfer ke" color={colors.Black} type="Text Bold 14" />
                <Gap height={16} />
                <View style={styles.selectBank}>
                    <View style={styles.imgBank}>
                        <Image source={BCA} alt="bca-image" style={{width: '100%', maxHeight: '100%'}} resizeMode="contain" />
                    </View>
                    <Gap width={16} />
                    <TextRegular text="BCA Virtual Account" type="Text Regular 12" color="#000" />
                </View>
                <View style={{marginTop: scaleSize(-16)}}>
                    <TextInput type="Copy" placeholder="098 7654 3210 9876" bold={true} placeholderColor={colors.Black} borderColor={'rgba(19, 129, 182, 0.1)'} />
                </View>
                <Gap height={12} />
                <Divider height={1} />
                <Gap height={12} />
                <TextRegular text="Lanjutkan Transfer ke" color={colors.Black} type="Text Regular 14" />
                <Gap height={12} />
                <View style={styles.total}>
                    <TextBold text={NumberFormatter(150000, 'IDR ')} type="Text Bold 16" color={colors.Black} />
                </View>
           </View>
            <Gap height={30} />
            <Divider height={4} />
            <Text style={styles.titleDetail2}>Panduan Pembayaran</Text>
            <Gap height={20} />
            <View style={styles.wrapperAccordion}>
                {
                    DataCollapse1.map((item) => {
                        return (
                            <TouchableOpacity key={item?.id} activeOpacity={0.8} style={styles.childAccordion} onPress={() => handleAccordion(item)}>
                            <View style={styles.childAccordionTop}>
                                <TextBold color={colors.Black} text={item?.title} />
                                {isShow === item?.id ? (
                                    <IcChevronUp fill="rgba(19, 129, 182, 1)" width={14} height={14} />

                                ) : (
                                    <IcChevronDown fill="rgba(19, 129, 182, 1)" width={14} height={14} />
                                )}
                            </View>
                            {isShow === item?.id && (
                                <>
                                    <Gap  height={20} />
                                    <TextRegular 
                                        text={item?.desc} 
                                        type="Text Regular 12" />
                                </>
                            )}
                       </TouchableOpacity>
                        )
                    })
                }
               <Gap height={30} />
               <View style={styles.wrapperBtn}>
                    <FilledButton onPress={() => navigation.navigate('ListOrder')} text="LIhat Daftar Pesanan" backgroundColor='#1381B6' textColor={colors.White} />
                    <Gap height={20} />
                    <FilledButton onPress={() => navigation.navigate('OtherPayment')} text="Ganti Metode Pembayaran" backgroundColor='rgba(19, 129, 182, 0.1)' textColor="#1381B6" />
               </View>
               <Gap height={30} />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White
    },
    cardTop2: {
        flexDirection: 'row',
        alignItems: 'center',
        height: scaleSize(80),
    },
    wrapperStep: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: scaleSize(20)   
    },
    step: {
        width: '33%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    numberStep2: () => ({
        borderWidth: scaleSize(1),
        borderColor: 'rgba(19, 129, 182, 1)',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(19, 129, 182, 1)',
        width: scaleSize(24),
        height: scaleSize(24),
        marginRight: scaleSize(4),
        borderRadius: scaleSize(90)
    }),
    numberStep: () => ({
        borderWidth: scaleSize(1),
        borderColor: '#434F65',
        alignItems: 'center',
        justifyContent: 'center',
        width: scaleSize(24),
        height: scaleSize(24),
        marginRight: scaleSize(4),
        borderRadius: scaleSize(90)
    }),
    titleDetail2: {
        fontSize: scaleSize(20),
        fontWeight: 'bold',
        marginTop: scaleSize(20),
        color: colors.Black,
        marginLeft: scaleSize(20)
    },
    cardNext: {
        borderRadius: scaleSize(10),
        padding: scaleSize(14),
        marginHorizontal: scaleSize(20),
        backgroundColor: colors.White,
        elevation: 3,
        height: 'auto',
    },
    cardCheck: {
        borderRadius: scaleSize(10),
        padding: scaleSize(14),
        marginHorizontal: scaleSize(20),
        backgroundColor: colors.White,
        elevation: 3,
        height: 'auto',
    },
    cardCheckTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: scaleSize(6)
    },
    wrapperTimes: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    childTimes: {
        width: scaleSize(22),
        height: scaleSize(22),
        borderRadius: scaleSize(4),
        backgroundColor: '#EB4335',
        alignItems: 'center',
        justifyContent: 'center',
    },
    imgCheckout: {
        width: scaleSize(80),
        height: '100%',
        borderRadius: scaleSize(10),
        overflow: 'hidden'
    },
    selectBank: {
        flexDirection: 'row',
        alignItems: 'center',
        height: scaleSize(50),
    },
    imgBank: {
        width: scaleSize(40),
        height: scaleSize(40),
        borderRadius: scaleSize(6),
        overflow: 'hidden',
    },
    total: {
        borderRadius: scaleSize(8),
        paddingVertical: scaleSize(12),
        backgroundColor: 'rgba(19, 129, 182, 0.1)',
        padding: scaleSize(20)
    },
    wrapperAccordion: {
        marginHorizontal: scaleSize(20),
        height: 'auto'
    },
    childAccordion: {
        elevation: 3,
        borderRadius: scaleSize(6),
        backgroundColor: colors.White,
        paddingVertical: scaleSize(20),
        paddingHorizontal: scaleSize(14),
        height: 'auto',
        marginBottom: scaleSize(16),
    },
    childAccordionTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
});

export default ContinuePayment2;