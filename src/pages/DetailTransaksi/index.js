import React from "react";
import { StyleSheet, ScrollView, StatusBar, FlatList, View, Image, TouchableOpacity } from "react-native";
import { Banner } from "../../assets/Dummy";
import { Divider, Gap, Header, TextBold, TextRegular } from "../../components";
import { colors, NumberFormatter, scaleSize } from "../../utils";

const DetailTransaki = () => {

    const listTransaksi = [
        {title: 'Jumlah tiket', value: '1 pcs'},
        {title: 'Tanggal pemesanan', value: '1 January 2023'},
        {title: 'Nama pelanggan', value: 'Shohei'},
        {title: 'Nomor handphone pelanggan', value: '0982176263276'},
        {title: 'Tanggal event', value: '30 January 2023, 07:17 WIB'},
        {title: 'Refund', value: 'Tidak bisa refund'},
    ]

    const renderItem = ({item}) => {
        return (
            <>
                <View style={styles.childList}>
                    <TextBold text={item?.title} color={colors.Black} type="Text Bold 14" />
                    <Gap height={8} />
                    <TextRegular text={item?.value} color="rgba(67, 79, 101, 1)" type="Text Regular 12" />
                </View>
                <Gap height={10} />
            </>
        )
    }

    return (
        <ScrollView>
            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <Header text="Detail Transaksi" />
            <View style={styles.cardEtiket}>
                <View style={styles.top}>
                    <View style={styles.cardEtiketLeft}>
                        <TextRegular text="ID:" color={colors.Black} type="Text Regular 14" />
                        <TextRegular text="#0987654" color={colors.Black} type="Text Regular 14" />
                    </View>
                    <View style={styles.cardEtiketRight}><TextRegular text="Selesai" color="rgba(52, 168, 83, 1)" type="Text Regular 12" /></View>
                </View>
                <Gap height={4} />
                <Divider height={1} />
                <Gap height={6} />
                <View style={styles.selectTiket}>
                    <View style={styles.imgSelectTiket}>
                        <Image source={Banner} alt="img" style={{width: '100%', height: '100%'}} resizeMode="cover" />
                    </View>
                    <Gap width={10} />
                    <View style={styles.rightSelect}>
                        <TextRegular text="Produk yang dipesan" color="rgba(67, 79, 101, 1)" type="Text Regular 12" />
                        <Gap height={8} />
                        <View style={styles.wrapperTitle}>
                            <View style={styles.titleSelectTiket}>
                                <TextBold text="National Musif Festival" color="#000" type="Text Bold 14" />
                            </View>
                            <TextBold text={NumberFormatter(1500000, 'IDR ')} color="rgba(19, 129, 182, 1)" type="Text Bold 14" />
                        </View>
                    </View>
                </View>
                <Gap height={20} />
                <View style={styles.listTransaksi}>
                    <FlatList 
                        data={listTransaksi}
                        renderItem={renderItem}
                    />
                </View>
            </View>
            <Gap height={40} />
            <View style={styles.footerDetailEvent}>
                <View style={styles.fdeLeft}>
                    <TextRegular text="Total" style={styles.start} />
                    <TextBold text={NumberFormatter(1500000, 'IDR ')} color="rgba(19, 129, 182, 1)" type="Text Bold 16" />
                </View>
                <TouchableOpacity activeOpacity={0.8} style={styles.btnfde}>
                    <TextBold text="LIhat E-Tiket" color={colors.White} type="Text Bold 12" />
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White,
    },
    cardEtiket: {
        marginHorizontal: scaleSize(0),
        borderRadius: scaleSize(8),
        height: 'auto',
        paddingBottom: scaleSize(10)
    },
    cardEtiketLeft: {
        width: 'auto',
        flexDirection: 'row',
        alignItems: 'center'
    },
    wrapperMenu: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 'auto',
        marginHorizontal: scaleSize(20)
    },
    cardEtiketRight: {
        borderRadius: scaleSize(88),
        backgroundColor: 'rgba(52, 168, 83, 0.1)',
        color: 'rgba(52, 168, 83, 1)',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: scaleSize(10),
        paddingVertical: scaleSize(4),
    },
    top: {
        flexDirection: 'row',
        padding: scaleSize(16),
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    selectTiket: {
        width: '90%',
        padding: scaleSize(10),
        backgroundColor: 'rgba(182, 198, 227, 0.15)',
        borderRadius: scaleSize(8),
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    imgSelectTiket: {
        width: scaleSize(60),
        maxHeight: scaleSize(60),
        borderRadius: scaleSize(8),
        overflow: 'hidden',
    },
    wrapperTitle: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
        paddingRight: scaleSize(8)
    },
    titleSelectTiket: {
        width: '50%',
        overflow: 'hidden'
    },
    listTransaksi: {
        width: '90%',
        marginLeft: 'auto',
        marginRight: 'auto',
        borderRadius: scaleSize(10),
        borderWidth: scaleSize(1),
        borderColor: 'rgba(227, 227, 227, 1)',
        paddingTop: scaleSize(16)
    },
    childList: {
        paddingHorizontal: scaleSize(20),
        borderBottomColor: 'rgba(227, 227, 227, 1)',
        borderBottomWidth: scaleSize(1),
        paddingBottom: scaleSize(12)
    },
    footerDetailEvent: {
        backgroundColor: colors.White,
        borderTopColor: '#EBEDF1',
        borderTopWidth: scaleSize(2),
        paddingBottom: scaleSize(30),
        paddingHorizontal: scaleSize(20),
        paddingVertical: scaleSize(14),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    fdeLeft: {
        width: '60%'
    },
    btnfde: {
        paddingHorizontal: scaleSize(34),
        paddingVertical: scaleSize(14),
        backgroundColor: '#1381B6',
        color: colors.White,
        borderRadius: scaleSize(8),
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3
    },
    textBtnfde: {
        color: colors.White,
    },
    textDiscountPrice: {
        color: 'red',
        fontSize: scaleSize(14),
        marginBottom: scaleSize(8)
    },
    start: {
        color: 'rgba(67, 79, 101, 1)',
        fontSize: scaleSize(14),
        marginBottom: scaleSize(4)
    },
});

export default DetailTransaki;