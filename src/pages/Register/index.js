import React from "react";
import { Dimensions, Image, ScrollView, StatusBar, StyleSheet, Text, View } from "react-native";
import { Logo } from "../../assets/Dummy";
import { Gap, Header, TextBold, TextInput, TextRegular } from "../../components";
import { FilledButton } from "../../components/Button";
import { colors } from "../../utils";

const Register = () => {
    return (
        <ScrollView style={styles.container}>
        <StatusBar
            barStyle="light-content"
            backgroundColor={colors.primary}
            />
        <Header />
        <Image 
            source={Logo}
            alt="Logo aplikasi"
            style={{
                marginLeft: 20,
                width: 200,
                marginTop: -40,
                maxHeight: 200,
            }}
            resizeMode="contain"
        />
        <View style={styles.wrapperTitle}>
            <TextBold color={colors.Black} text="Buat akun baru" type="Title Bold 28" style={styles.titleLogin} />
            <TextRegular style={styles.pLogin} type="Text Reguler 16" color="#434F65" text="Buat akun baru etiket" />
        </View>

        <View style={styles.wrapperInput}>
            <TextInput label="Nama" placeholder="Nama" />
            <Gap height={20} />
            <TextInput type="phone" label="Nomor Telepon" placeholder="Nomer Telepon" />
            <Gap height={20} />
            <TextInput label="Email" placeholder="Email" />
            <Gap height={20} />
            <TextInput type="password" label="Password" placeholder="Password" />
        </View>
        
        <View style={styles.wrapperBtnLogin}>
            <FilledButton style={styles.btnLogin} text="Masuk" />
        </View>

        <View style={styles.wrapperText}>
            <Text style={styles.textNotHave}>Belum punya akun?</Text>
            <Text style={styles.textCreateNew}>Buat akun baru</Text>
        </View>

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White,
    },
    wrapperText: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        textAlign: 'center',
        marginTop: 24,
        marginBottom: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textCreateNew: {
        marginLeft: 4,
        fontWeight: 'bold',
        color: '#1381B6'
    },
    wrapperBtnLogin: {
        marginTop: 40,
        marginHorizontal: 20
    },
    wrapperInput: {
        marginTop: 32,
        marginHorizontal: 20
    },
    titleLogin: {
        fontWeight: 'bold',
        marginLeft: 20,
        marginBottom: 6,
    },
    pLogin: {
        width: '90%',
        marginLeft: 20,
    },
    wrapperTitle: {
        marginTop: -30,
    }
});

export default Register;