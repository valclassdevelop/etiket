import React, { useState } from 'react';
import { FlatList, ScrollView, Image, StatusBar, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Banner } from '../../assets/Dummy';
import { Gap, Header, Divider, TextRegular, TextBold, FilledButton } from '../../components';
import { colors, NumberFormatter, scaleSize } from '../../utils';

const MyOrder = ({navigation}) => {
    const [active, setActive] = useState("Semua");
    const dataMenu = ['Semua', 'Belum Dibayar', 'Lunas', 'Kadaluarsa'];
    const [expired1, setExpired1] = useState(false);
    const [expired2, setExpired2] = useState(true);

    return (
        <ScrollView style={styles.container}>
            <StatusBar
                barStyle="dark-content"
                backgroundColor={colors.White}
            />
            <Header text="Pesanan Saya" onBack={() => navigation.goBack(-1)} />
            <Gap height={20} />
            <View style={styles.wrapperMenu}>
                <FlatList 
                    data={dataMenu}
                    horizontal
                    pagingEnabled={false}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({item}) => {
                        return (
                            <TouchableOpacity activeOpacity={0.8} onPress={() => setActive(item)} style={styles.childMenu(active, item)}>
                                <TextRegular text={item} type="Text Regular 12" color={active == item ? colors.White : 'rgba(67, 79, 101, 1)'} />
                          </TouchableOpacity>
                        )
                    }}
                />
            </View>
            <Gap height={20} />
            <View style={styles.cardEtiket}>
                <View style={styles.top}>
                    <View style={styles.cardEtiketLeft}>
                        <TextBold text="ID Pesanan : " color={colors.Black} type="Text Bold 14" />
                        <TextBold text="#0987654" color={colors.Black} type="Text Bold 14" />
                    </View>
                </View>
                <Gap height={4} />
                <Divider height={1} />
                <Gap height={12} />
                <View style={styles.selectTiket}>
                    <View style={styles.imgSelectTiket}>
                        <Image source={Banner} alt="img" style={{width: '100%', height: '100%'}} resizeMode="cover" />
                    </View>
                    <Gap width={10} />
                    <View style={styles.rightSelect}>
                        <TextRegular text="Event" color="rgba(67, 79, 101, 1)" type="Text Regular 12" />
                        <View style={styles.wrapperTitle}>
                            <View style={styles.titleSelectTiket}>
                                <TextBold text="National Musif Festival" color="#000" type="Text Bold 16" />
                            </View>
                        </View>
                    </View>
                </View>
                <Divider height={1} />
                <Gap height={20} />
                <View style={styles.wrapperPrice}>
                    <TextRegular text="Harga Tiket" color="rgba(67, 79, 101, 1)" type="Text Regular 14" />
                    <TextBold text={NumberFormatter(1500000, 'IDR ')} color={colors.Black} type="Text Bold 14" />
                </View>
                <Gap height={14} />
                <Divider height={2} />
                <Gap height={14} />
                <View style={{marginHorizontal: scaleSize(20)}}>
                    {
                        expired1 ? (
                            <FilledButton text="Waktu Pembayaran Kadaluarsa" backgroundColor='rgba(243, 251, 255, 1)' textColor="rgba(182, 198, 227, 1)" />
                            ):
                            <FilledButton text="Selesaikan Pembayaran 00:50:49" backgroundColor='rgba(19, 129, 182, 1)' textColor={colors.White} />
                    }
                </View>
            </View>
            <Gap height={20} />
            <View style={styles.cardEtiket}>
                <View style={styles.top}>
                    <View style={styles.cardEtiketLeft}>
                        <TextBold text="ID Pesanan : " color={colors.Black} type="Text Bold 14" />
                        <TextBold text="#0987654" color={colors.Black} type="Text Bold 14" />
                    </View>
                </View>
                <Gap height={4} />
                <Divider height={1} />
                <Gap height={12} />
                <View style={styles.selectTiket}>
                    <View style={styles.imgSelectTiket}>
                        <Image source={Banner} alt="img" style={{width: '100%', height: '100%'}} resizeMode="cover" />
                    </View>
                    <Gap width={10} />
                    <View style={styles.rightSelect}>
                        <TextRegular text="Event" color="rgba(67, 79, 101, 1)" type="Text Regular 12" />
                        <View style={styles.wrapperTitle}>
                            <View style={styles.titleSelectTiket}>
                                <TextBold text="National Musif Festival" color="#000" type="Text Bold 14" />
                            </View>
                        </View>
                    </View>
                </View>
                <Divider height={1} />
                <Gap height={20} />
                <View style={styles.wrapperPrice}>
                    <TextRegular text="Harga Tiket" color="rgba(67, 79, 101, 1)" type="Text Regular 14" />
                    <TextBold text={NumberFormatter(1500000, 'IDR ')} color={colors.Black} type="Text Bold 14" />
                </View>
                <Gap height={14} />
                <Divider height={2} />
                <Gap height={14} />
                <View style={{marginHorizontal: scaleSize(20)}}>
                {
                    expired2 ? (
                        <FilledButton text="Waktu Pembayaran Kadaluarsa" backgroundColor='rgba(243, 251, 255, 1)' textColor="rgba(182, 198, 227, 1)" />
                        ):
                        <FilledButton text="Selesaikan Pembayaran 00:50:49" backgroundColor='rgba(19, 129, 182, 1)' textColor={colors.White} />
                }
                </View>
            </View>
            <Gap height={30} />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.White
    },
    wrapperMenu: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 'auto',
        marginLeft: scaleSize(20)
    },
    rightSelect: {
        justifyContent: 'center',
        paddingTop: scaleSize(8)
    },
    childMenu: (active, item) => ({
        borderRadius: scaleSize(99),
        width: 'auto',
        marginRight: scaleSize(10),
        height: 'auto',
        borderWidth: scaleSize(1),
        borderColor: 'rgba(235, 237, 241, 1)',
        paddingHorizontal: scaleSize(14),
        paddingVertical: scaleSize(6),
        alignItems: 'center',
        backgroundColor: active == item ? 'rgba(19, 129, 182, 1)' : colors.White,
    }),
    cardEtiket: {
        backgroundColor: colors.White,
        elevation: 3,
        marginHorizontal: scaleSize(20),
        borderRadius: scaleSize(8),
        height: 'auto',
        paddingBottom: scaleSize(16)
    },
    cardEtiketLeft: {
        width: 'auto',
        flexDirection: 'row',
        alignItems: 'center'
    },
    wrapperMenu: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 'auto',
        marginHorizontal: scaleSize(20)
    },
    cardEtiketRight: {
        borderRadius: scaleSize(88),
        backgroundColor: 'rgba(52, 168, 83, 0.1)',
        color: 'rgba(52, 168, 83, 1)',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: scaleSize(10),
        paddingVertical: scaleSize(4),
    },
    top: {
        flexDirection: 'row',
        padding: scaleSize(16),
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    selectTiket: {
        width: '90%',
        padding: scaleSize(10),
        backgroundColor: 'rgba(182, 198, 227, 0.15)',
        borderRadius: scaleSize(8),
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    imgSelectTiket: {
        width: scaleSize(60),
        maxHeight: scaleSize(60),
        borderRadius: scaleSize(8),
        overflow: 'hidden',
    },
    wrapperTitle: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
        paddingRight: scaleSize(8),
        alignItems: 'center'
    },
    titleSelectTiket: {
        width: '90%',
        overflow: 'hidden'
    },
    descSelect: {
        justifyContent: 'space-between',
        marginHorizontal: scaleSize(20),
        flexDirection: 'row',
        alignItems: 'center',
    },
    wrapperPrice: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: scaleSize(20)
    }
});

export default MyOrder;