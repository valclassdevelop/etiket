import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {BottomNavigation} from '../components';
import {
  CreatePassword,
  DetailEtiket,
  ListOrder,
  ShowOrder,
  DetailOrder,
  DetailEvent,
  Event,
  ForgotPassword,
  Homepage,
  Login,
  OnBoarding,
  Otp,
  Profile,
  Register,
  Transaksi,
  Checkout,
  OtherPayment,
  ContinuePayment1,
  ContinuePayment2,
  PaymentSuccess,
  DetailTransaksi,
  EditProfile,
  MyOrder,
  FaqService,
  CallService,
  Etiket,
  Notification,
} from '../pages';
import UpdatePassword from '../pages/UpdatePassword';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={props => <BottomNavigation {...props} />}>
      <Tab.Screen
        name="Homepage"
        component={Homepage}
        options={{
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="Event"
        component={Event}
        options={{
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="Etiket"
        component={Etiket}
        options={{
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="Transaksi"
        component={Transaksi}
        options={{
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};

const Router = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        headerShown: false,
      }}>
      <Stack.Screen name="OnBoarding" component={OnBoarding} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="MainApp" component={MainApp} />
      <Stack.Screen name="Transaksi" component={Transaksi} />
      <Stack.Screen name="FaqService" component={FaqService} />
      <Stack.Screen name="Notification" component={Notification} />
      <Stack.Screen name="Etiket" component={Etiket} />
      <Stack.Screen name="DetailOrder" component={DetailOrder} />
      <Stack.Screen name="ShowOrder" component={ShowOrder} />
      <Stack.Screen name="Homepage" component={Homepage} />
      <Stack.Screen name="MyOrder" component={MyOrder} />
      <Stack.Screen name="CallService" component={CallService} />
      <Stack.Screen name="UpdatePassword" component={UpdatePassword} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="DetailTransaksi" component={DetailTransaksi} />
      <Stack.Screen name="DetailEtiket" component={DetailEtiket} />
      <Stack.Screen name="ListOrder" component={ListOrder} />
      <Stack.Screen name="PaymentSuccess" component={PaymentSuccess} />
      <Stack.Screen name="ContinuePayment1" component={ContinuePayment1} />
      <Stack.Screen name="Checkout" component={Checkout} />
      <Stack.Screen name="ContinuePayment2" component={ContinuePayment2} />
      <Stack.Screen name="OtherPayment" component={OtherPayment} />
      <Stack.Screen name="DetailEvent" component={DetailEvent} />
      <Stack.Screen name="Event" component={Event} />
      <Stack.Screen name="CreatePassword" component={CreatePassword} />
      <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Otp" component={Otp} />
    </Stack.Navigator>
  );
};

export default Router;
