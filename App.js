import {NavigationContainer} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {LogBox} from 'react-native';
import {SplashScreen} from './src/modules';
import Router from './src/route';

// Ignore log notification by message
LogBox.ignoreLogs(['Warning: ...']);

//Ignore all log notificationsimport { LogBox } from 'react-native';

// Ignore log notification by message
LogBox.ignoreLogs(['Warning: ...']);

//Ignore all log notifications
LogBox.ignoreAllLogs();
LogBox.ignoreAllLogs();

function App() {
  const [isLogin] = useState(false);

  useEffect(() => {
    if (isLogin) {
      SplashScreen.hide();
    } else {
      SplashScreen.hide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <NavigationContainer>
      <Router />
    </NavigationContainer>
  );
}

export default App;
